package dk.s4.microservices.patientcareservice.servlet;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.api.ServerValidationModeEnum;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.MessagingUtils;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.ResourceUtil;
import dk.s4.microservices.patientcareservice.ServiceVariables;
import dk.s4.microservices.patientcareservice.Utils.ResultCaptor;
import dk.s4.microservices.patientcareservice.messaging.MyEventProcessor;
import dk.s4.microservices.patientcareservice.messaging.ResourceDeleter;
import org.eclipse.jetty.server.Server;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CarePlan;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Patient;
import org.junit.*;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.UUID;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static junit.framework.TestCase.fail;
import static org.mockito.Mockito.*;

public class RestfulFHIRProvidersTest {

    private static IGenericClient ourClient;
    private static final org.slf4j.Logger ourLog = org.slf4j.LoggerFactory.getLogger(RestfulFHIRProvidersTest.class);
    private static IParser jsonParser;

    private static String FHIR_CAREPLAN_FOR_QUESTIONNAIRE = "FHIR-clean/CarePlan.json";
    private static String FHIR_CAREPLAN_FOR_OBSDEF = "careplan_obsdef.json";
    private static String FHIR_PATIENT = "FHIR-clean/Patient.json";
    private static String subjectIdentifier = "2512489996";
    private static String organizationIdentifier = "453191000016003";

    private static int ourPort;

    private static Server ourServer;
    private static String ourServerBase;


    @Rule
    public WireMockRule wireMockRule = new WireMockRule(wireMockConfig().dynamicPort());

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    private void setupWireMockForBrugsAftale() {
        String url = "http://localhost:" + wireMockRule.port() + "/";
        environmentVariables.set("BRUGS_SERVICE_URL", url);


        stubFor(post(urlEqualTo("/api/de67e5c1-494a-41b9-924d-4c613b076a73"))
                .willReturn(aResponse()
                        .withStatus(201)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"resourceType\": \"Brugsaftale\",\"id\": \"42\"}")));
    }

    @Test
    public void testProcessAndCreateOrUpdateCarePlan() throws IOException {
        ourLog.debug("testProcessAndCreateOrUpdateCarePlan");

        setupWireMockForBrugsAftale();

        String resourceString = ResourceUtil.stringFromResource(FHIR_CAREPLAN_FOR_QUESTIONNAIRE);
        CarePlan carePlan = (CarePlan) PatientCareService.getServerFhirContext().newJsonParser().parseResource(resourceString);
        String subjectIdentifier = UUID.randomUUID().toString();
        carePlan.getSubject().getIdentifier().setValue(subjectIdentifier);

        String correlationId = MessagingUtils.verifyOrCreateId(null);
        String transactionId = UUID.randomUUID().toString();

        Topic consumedTopic = new Topic().
                setOperation(Topic.Operation.Create).
                setDataCategory(Topic.Category.FHIR).
                setDataType("CarePlan");

        Topic processedTopic = new Topic().
                setOperation(Topic.Operation.DataCreated).
                setDataCategory(Topic.Category.FHIR).
                setDataType("CarePlan");

        Message message = new Message().
                setSender("EmployeeBffTest").
                setBodyCategory(Message.BodyCategory.FHIR).
                setBodyType("CarePlan").
                setContentVersion(ServiceVariables.FHIR_VERSION.getValue()).
                setCorrelationId(correlationId).
                setTransactionId(transactionId).
                setBody(PatientCareService.getServerFhirContext().newJsonParser().encodeResourceToString(carePlan));

        Message outgoingMessage = new Message();

        ResourceDeleter<Patient> resourceDeleter = new ResourceDeleter<Patient>(PatientCareService.getPatientDao(),PatientCareService.getDaoConfig());
        ResourceDeleter<CarePlan> carePlanDeleter = new ResourceDeleter<CarePlan>(PatientCareService.getCarePlanDao(),PatientCareService.getDaoConfig());
        MyEventProcessor myEventProcessor = new MyEventProcessor(PatientCareService.getServerFhirContext(),
                PatientCareService.getBasicDao(), PatientCareService.getCarePlanDao(), PatientCareService.getPatientDao(), resourceDeleter, carePlanDeleter);
        Assert.assertTrue(myEventProcessor.processCreateMessage(consumedTopic, carePlan, message, processedTopic, outgoingMessage));
    }


    @Test
    public void testConsentWithdrawal() throws Exception {

        setupWireMockForBrugsAftale();
        String userId = UUID.randomUUID().toString();
        IIdType carePlan = createAndStoreCareplan(userId);
        IIdType patient = createAndStorePatient(userId);
        ObjectNode node = createNode(userId);

        Message message = new Message().
                setSender("OutcomeServiceTest").
                setBodyCategory(Message.BodyCategory.System).
                setCorrelationId(MessagingUtils.verifyOrCreateId(null)).
                setTransactionId(UUID.randomUUID().toString()).
                setBody(node.toString()).
                setContentVersion(System.getenv("FHIR_VERSION"));
        Topic inputTopic = new Topic()
                .setOperation(Topic.Operation.Delete)
                .setDataCategory(Topic.Category.System)
                .setDataType("ConsentDeleted");
        Topic outputTopic = new Topic()
                .setOperation(Topic.Operation.ProcessingOk)
                .setDataCategory(Topic.Category.System)
                .setDataType("ConsentDeleted");

        ResourceDeleter<Patient> patientDeleter = new ResourceDeleter<Patient>(PatientCareService.getPatientDao(),PatientCareService.getDaoConfig());
        ResourceDeleter<Patient> spyResourceDeleter = spy(patientDeleter);
        ResultCaptor<Future<?>> patientDeletionResultCaptor = new ResultCaptor<>();
        doAnswer(patientDeletionResultCaptor).when(spyResourceDeleter).deleteResourceByUserIdentifier(userId);

        ResourceDeleter<CarePlan> carePlanDeleter = new ResourceDeleter<CarePlan>(PatientCareService.getCarePlanDao(),PatientCareService.getDaoConfig());
        ResourceDeleter<CarePlan> spyCarePlanDeleter = spy(carePlanDeleter);
        ResultCaptor<Future<?>> carePlanDeletionResultCaptor = new ResultCaptor<>();
        doAnswer(carePlanDeletionResultCaptor).when(spyCarePlanDeleter).deleteResourceByUserIdentifier(userId);


        MyEventProcessor myEventProcessor = new MyEventProcessor(PatientCareService.getServerFhirContext(),
                PatientCareService.getBasicDao(), PatientCareService.getCarePlanDao(), PatientCareService.getPatientDao(), spyResourceDeleter, spyCarePlanDeleter);
        Assert.assertTrue(myEventProcessor.processDelete(inputTopic, message, outputTopic, new Message()));

        Future<?> deletePatientsFuture = patientDeletionResultCaptor.getResult();
        deletePatientsFuture.get(1000, TimeUnit.MILLISECONDS);

        Future<?> deleteCarePlansFuture = carePlanDeletionResultCaptor.getResult();
        deleteCarePlansFuture.get(1000,TimeUnit.MILLISECONDS);

        try{
            PatientCareService.getCarePlanDao().read(carePlan);
            PatientCareService.getPatientDao().read(patient);
            fail();
        }
        catch (Exception e){
            // good
        }
    }

    private ObjectNode createNode(String userId){
        ObjectNode node = new ObjectMapper().createObjectNode();
        node.put("userId", userId);
        node.put("consentSpecId", "someConsentSpecId");
        node.put("timestamp", Instant.now().toString());
        return node;
    }

    private IIdType createAndStorePatient(String userId){
        Patient p = new Patient().addIdentifier(new Identifier().setValue(userId).setSystem(System.getenv("OFFICIAL_PATIENT_IDENTIFIER_SYSTEM")));
        return PatientCareService.getPatientDao().create(p).getId();
    }

    private IIdType createAndStoreCareplan(String userId) throws IOException{
        String resourceString = ResourceUtil.stringFromResource(FHIR_CAREPLAN_FOR_QUESTIONNAIRE);
        CarePlan carePlan = (CarePlan) PatientCareService.getServerFhirContext().newJsonParser().parseResource(resourceString);
        String subjectIdentifier = userId;
        carePlan.getSubject().getIdentifier().setValue(subjectIdentifier);
        carePlan.getSubject().getIdentifier().setSystem(System.getenv("OFFICIAL_PATIENT_IDENTIFIER_SYSTEM"));
        return PatientCareService.getCarePlanDao().create(carePlan).getId();
    }

    @Test
    public void testGetActiveForPatientNoneActive() {
        ourLog.debug("testGetActiveForPatientNoneActive");
        String searchUrl = ourServerBase + "/CarePlan?_query=getActiveForPatient&identifier=urn:oid:1.2.208.176.1.2|99999";

        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testGetActiveForPatient() {
        ourLog.debug("testGetActiveForPatient");

        String searchUrl = ourServerBase + "/CarePlan?_query=getActiveForPatient&identifier=urn:oid:1.2.208.176.1.2|" + subjectIdentifier;

        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testGetActiveForPatientWithAuthor() {
        ourLog.debug("testGetActiveForPatientWithAuthor");

        String searchUrl = ourServerBase + "/CarePlan?_query=getActiveForPatient&identifier=urn:oid:1.2.208.176.1.2|" + subjectIdentifier +
                "&author=urn:oid:1.2.208.176.1.1|" + organizationIdentifier;

        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testGetActiveForPatientInvalidIdentifier() {
        ourLog.debug("testGetActiveForPatientInvalidIdentifier");

        String searchUrl = ourServerBase + "/CarePlan?_query=getActiveForPatient&identifier=urn:oid:1.2.208.176.1.2|invalidValue";

        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testGetActiveForPatientWithInvalidAuthor() {
        ourLog.debug("testUpdateOrganization");

        String searchUrl = ourServerBase + "/CarePlan?_query=getActiveForPatient&identifier=urn:oid:1.2.208.176.1.2|" + subjectIdentifier +
                "&author=urn:oid:1.2.208.176.1.1|invalidIdentifier";

        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testGetAllForPatient() {
        ourLog.debug("testGetAllForPatient");

        String searchUrl = ourServerBase + "/CarePlan?_query=getAllForPatient&identifier=urn:oid:1.2.208.176.1.2|" + subjectIdentifier;

        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testGetAllForPatientWithAuthor() {
        ourLog.debug("testGetAllForPatientWithAuthor");

        String searchUrl = ourServerBase + "/CarePlan?_query=getAllForPatient&identifier=urn:oid:1.2.208.176.1.2|" + subjectIdentifier +
                "&author=urn:oid:1.2.208.176.1.1|" + organizationIdentifier;

        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testGetAllForPatientInvalidIdentifier() {
        ourLog.debug("testGetAllForPatientInvalidIdentifier");

        String searchUrl = ourServerBase + "/CarePlan?_query=getAllForPatient&identifier=urn:oid:1.2.208.176.1.2|invalidValue";

        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testGetAllForPatientWithInvalidAuthor() {
        ourLog.debug("testGetAllForPatientWithInvalidAuthor");

        String searchUrl = ourServerBase + "/CarePlan?_query=getAllForPatient&identifier=urn:oid:1.2.208.176.1.2|" + subjectIdentifier +
                "&author=urn:oid:1.2.208.176.1.1|invalidValue";

        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testGetActiveForPatient10Times() {
        ourLog.debug("testGetActiveForPatient10Times");

        String searchUrl = ourServerBase + "/CarePlan?_query=getActiveForPatient&identifier=urn:oid:1.2.208.176.1.2|" + subjectIdentifier;

        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        for (int i = 0; i < 10; i++) {
            Bundle result = query.execute();
            Assert.assertTrue(result.hasEntry());
        }
    }

    @Test
    public void testSearchByIdentifier() {
        ourLog.debug("testSearchByIdentifier");

        Patient patient = new Patient();
        patient.addIdentifier(new Identifier().setValue("2512489996"));
        PatientCareServiceFacade.getPatientDao().create(patient);

        String searchUrl = ourServerBase +
                "/Patient?_query=searchByIdentifier&" +
                "identifier=2512489996";

        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testGenericCarePlanSearch() {
        ourLog.debug("testGenericCarePlanSearch");
        String searchUrl = ourServerBase +
                "/CarePlan?";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testGenericCarePlanSearchWithIdentifierArgument() {
        ourLog.debug("testGenericCarePlanSearchWithIdentifierArgument");
        String searchUrl = ourServerBase +
                "/CarePlan?identifier=urn:ietf:rfc:3986|de67e5c1-494a-41b9-924d-4c613b076a73";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testGenericCarePlanSearchWithInvalidIdentifierArgument() {
        ourLog.debug("testGenericCarePlanSearchWithInvalidIdentifierArgument");
        String searchUrl = ourServerBase +
                "/CarePlan?identifier=urn:ietf:rfc:3986|invalidIdentifier";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testGenericCarePlanSearchWithStatusArgument() {
        ourLog.debug("testGenericCarePlanSearchWithStatusArgument");
        String searchUrl = ourServerBase +
                "/CarePlan?status=active";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testGenericCarePlanSearchWithInvalidStatusArgument() {
        ourLog.debug("testGenericCarePlanSearchWithInvalidStatusArgument");
        String searchUrl = ourServerBase +
                "/CarePlan?status=unknown";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testGenericCarePlanSearchWithSubjectArgument() {
        ourLog.debug("testGenericCarePlanSearchWithSubjectArgument");
        String searchUrl = ourServerBase +
                "/CarePlan?subjectIdentifier=urn:oid:1.2.208.176.1.2|2512489996";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testGenericCarePlanSearchWithInvalidSubjectIdentifierValueArgument() {
        ourLog.debug("testGenericCarePlanSearchWithInvalidSubjectIdentifierValueArgument");
        String searchUrl = ourServerBase +
                "/CarePlan?subjectIdentifier=urn:oid:1.2.208.176.1.2|invalidIdentifier";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testGenericCarePlanSearchWithInvalidSubjectIdentifierSystemArgument() {
        ourLog.debug("testGenericCarePlanSearchWithInvalidSubjectIdentifierSystemArgument");
        String searchUrl = ourServerBase +
                "/CarePlan?subjectIdentifier=invalidSystem|2512489996";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testGenericCarePlanSearchWithAuthorArgument() {
        ourLog.debug("testGenericCarePlanSearchWithAuthorArgument");
        String searchUrl = ourServerBase +
                "/CarePlan?authorIdentifier=urn:oid:1.2.208.176.1.1|453191000016003";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testGenericCarePlanSearchWithInvalidAuthorIdentifierValueArgument() {
        ourLog.debug("testGenericCarePlanSearchWithInvalidAuthorIdentifierValueArgument");
        String searchUrl = ourServerBase +
                "/CarePlan?authorIdentifier=urn:oid:1.2.208.176.1.1|invalidIdentifier";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testGenericCarePlanSearchWithInvalidAuthorIdentifierSystemArgument() {
        ourLog.debug("testGenericCarePlanSearchWithInvalidAuthorIdentifierSystemArgument");
        String searchUrl = ourServerBase +
                "/CarePlan?authorIdentifier=invalidSystem|453191000016003";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testGetConnectedToOrganization() {
        ourLog.debug("testGetConnectedToOrganization");
        String searchUrl = ourServerBase +
                "/Patient?_query=getConnectedToOrganization" +
                "&authorIdentifier=urn:oid:1.2.208.176.1.1|453191000016003";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testGetConnectedToOrganizationInvalidIdentifierValue() {
        ourLog.debug("testGetConnectedToOrganization");
        String searchUrl = ourServerBase +
                "/Patient?_query=getConnectedToOrganization" +
                "&authorIdentifier=urn:oid:1.2.208.176.1.1|invalidValue";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        query.execute();
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testGetConnectedToOrganizationInvalidIdentifierSystem() {
        ourLog.debug("testGetConnectedToOrganization");
        String searchUrl = ourServerBase +
                "/Patient?_query=getConnectedToOrganization" +
                "&authorIdentifier=invalidSystem|453191000016003";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        query.execute();
    }

    @BeforeClass
    public static void beforeClass() throws Exception {
        String value = System.getenv("DATABASE_TYPE");
        if (value == null) {
            environmentVariables.set("DATABASE_TYPE", "Derby");
        }

        /*
         * This runs under maven, and I'm not sure how else to figure out the target directory from code..
         */
        String path = RestfulFHIRProvidersTest.class.getClassLoader().getResource(".keep").getPath();
        path = new File(path).getParent();
        path = new File(path).getParent();
        path = new File(path).getParent();

        TestUtils.readEnvironment("service.env", new EnvironmentVariables());
        TestUtils.readEnvironment("deploy.env", new EnvironmentVariables());

        ourLog.info("Project base path is: {}", path);

        //Disable authentication
        environmentVariables.set("ENABLE_AUTHENTICATION", "false");
        environmentVariables.set("ENABLE_DIAS_AUTHENTICATION", "false");

        //Init web server and start it
        ourPort = TestUtils.findFreePort();
        ourServer = new Server(ourPort);
        ourServerBase = TestUtils.initWebServer(path + "/src/main/webapp/WEB-INF/without-keycloak/web.xml", path + "/target/service", environmentVariables, ourPort, ourServer);


        PatientCareService.getServerFhirContext().getRestfulClientFactory().setServerValidationMode(ServerValidationModeEnum.NEVER);
        PatientCareService.getServerFhirContext().getRestfulClientFactory().setSocketTimeout(1200 * 1000);
        FhirContext fhirContext = PatientCareService.getServerFhirContext();
        ourClient = fhirContext.newRestfulGenericClient(ourServerBase);


        // Populate database
        jsonParser = PatientCareService.getServerFhirContext().newJsonParser();

        // Careplan referencing Questionnaire
        String questionnaireCareplanString = ResourceUtil.stringFromResource(FHIR_CAREPLAN_FOR_QUESTIONNAIRE);
        CarePlan questionnaireCareplan = jsonParser.parseResource(CarePlan.class, questionnaireCareplanString);
        PatientCareService.getCarePlanDao().create(questionnaireCareplan);

        // Careplan referencing ObservationDefinition
        String obsDefCareplanString = ResourceUtil.stringFromResource(FHIR_CAREPLAN_FOR_OBSDEF);
        CarePlan obsDefCareplan = jsonParser.parseResource(CarePlan.class, obsDefCareplanString);
        PatientCareService.getCarePlanDao().create(obsDefCareplan);

        // Patient
        String patientString = ResourceUtil.stringFromResource(FHIR_PATIENT);
        Patient patient = jsonParser.parseResource(Patient.class, patientString);
        patient.getIdentifierFirstRep().setSystem(ServiceVariables.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.getValue());
        PatientCareService.getPatientDao().create(patient);

        environmentVariables.set("ENABLE_KAFKA", "false");
    }

    @AfterClass
    public static void afterClass() throws Exception {
        ourServer.stop();
    }
}
