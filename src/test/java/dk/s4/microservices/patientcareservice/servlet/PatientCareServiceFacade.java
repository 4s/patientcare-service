package dk.s4.microservices.patientcareservice.servlet;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import org.hl7.fhir.r4.model.Basic;
import org.hl7.fhir.r4.model.CarePlan;
import org.hl7.fhir.r4.model.Patient;

public class PatientCareServiceFacade {

    /**
     * For test
     */
    public static IFhirResourceDao<CarePlan> getCarePlanDao() {
        return PatientCareService.getCarePlanDao();
    }

    /**
     * For test
     */
    public static IFhirResourceDao<Patient> getPatientDao() {
        return PatientCareService.getPatientDao();
    }

    /**
     * For test
     */
    static IFhirResourceDao<Basic> getBasicDao() {
        return PatientCareService.getBasicDao();
    }

    /**
     * For testing
     */
    public static FhirContext getServerFhirContext() {
        return PatientCareService.getServerFhirContext();
    }
}
