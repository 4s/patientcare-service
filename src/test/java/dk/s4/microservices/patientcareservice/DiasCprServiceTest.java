package dk.s4.microservices.patientcareservice;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.patientcareservice.servlet.RestfulFHIRProvidersTest;
import java.io.File;
import java.util.Calendar;
import java.util.Date;
import org.hl7.fhir.r4.model.Address.AddressUse;
import org.hl7.fhir.r4.model.Enumerations.AdministrativeGender;
import org.hl7.fhir.r4.model.Patient;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

public class DiasCprServiceTest {

    private final static String nancyFromCprService = "{\n"
            + "  \"patient\":{\n"
            + "    \"resourceType\": \"Patient\",\n"
            + "    \"identifier\": [\n"
            + "      {\n"
            + "        \"id\": \"2512489996\",\n"
            + "        \"system\": \"1.2.208.176.1.2\"\n"
            + "      }\n"
            + "    ],\n"
            + "    \"name\": [\n"
            + "      {\n"
            + "        \"family\": \"Berggren\",\n"
            + "        \"given\": [\n"
            + "          \"Nancy\",\n"
            + "          \"Ann\",\n"
            + "          \"Test\"\n"
            + "        ]\n"
            + "      }\n"
            + "    ],\n"
            + "    \"gender\": \"female\",\n"
            + "    \"birthDate\": \"1948-12-25\",\n"
            + "    \"address\": [\n"
            + "      {\n"
            + "        \"line\": [\n"
            + "          \"Testpark Allé 48\"\n"
            + "        ],\n"
            + "        \"city\": \"Hillerød\",\n"
            + "        \"postalCode\": \"3400\",\n"
            + "        \"country\": \"Danmark\"\n"
            + "      }\n"
            + "    ]\n"
            + "  }\n"
            + "}";
    private final static String mayJuneFromCprService = "{\n"
            + "  \"patient\":{\n"
            + "    \"resourceType\": \"Patient\",\n"
            + "    \"identifier\": [\n"
            + "      {\n"
            + "        \"id\": \"0108629996\",\n"
            + "        \"system\": \"1.2.208.176.1.2\"\n"
            + "      }\n"
            + "    ],\n"
            + "    \"name\": [\n"
            + "      {\n"
            + "        \"family\": \"Moberg\",\n"
            + "        \"given\": [\n"
            + "          \"May\",\n"
            + "          \"June\",\n"
            + "          \"Test\"\n"
            + "        ]\n"
            + "      }\n"
            + "    ],\n"
            + "    \"gender\": \"female\",\n"
            + "    \"birthDate\": \"1962-08-01\",\n"
            + "    \"address\": [\n"
            + "      {\n"
            + "        \"line\": [\n"
            + "          \"Testgrusgraven 3\",\n"
            + "          \"3.tv\"\n"
            + "        ],\n"
            + "        \"city\": \"Hillerød\",\n"
            + "        \"postalCode\": \"3400\",\n"
            + "        \"country\": \"Danmark\"\n"
            + "      }\n"
            + "    ]\n"
            + "  }\n"
            + "}";

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(wireMockConfig().dynamicPort());

    @Test
    public void getPatientNancy() throws Exception {
        DiasCprService cprService = new DiasCprService(setupWireMockForCprService());
        Patient patient = cprService.getPatient("2512489996");
        Assert.assertTrue(patient.hasActive() && patient.getActive());
        Assert.assertEquals(1, patient.getIdentifier().size());
        Assert.assertEquals("2512489996", patient.getIdentifier().get(0).getValue());
        Assert.assertEquals(ServiceVariables.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.getValue(), patient.getIdentifier().get(0).getSystem());
        Assert.assertEquals(1, patient.getName().size());
        Assert.assertEquals("Berggren", patient.getName().get(0).getFamily());
        Assert.assertEquals(3, patient.getName().get(0).getGiven().size());
        Assert.assertEquals("Nancy Ann Test", patient.getName().get(0).getGivenAsSingleString());
        Assert.assertEquals(AdministrativeGender.FEMALE, patient.getGender());
        Assert.assertEquals(48, patient.getBirthDate().getYear());
        Assert.assertEquals(11, patient.getBirthDate().getMonth());
        Assert.assertEquals(25, patient.getBirthDate().getDate());
        Assert.assertEquals(1, patient.getAddress().size());
        Assert.assertEquals(1, patient.getAddress().get(0).getLine().size());
        Assert.assertEquals("Testpark Allé 48", patient.getAddress().get(0).getLine().get(0).toString());
        Assert.assertEquals("Hillerød", patient.getAddress().get(0).getCity());
        Assert.assertEquals("3400", patient.getAddress().get(0).getPostalCode());
        Assert.assertEquals("Danmark", patient.getAddress().get(0).getCountry());
        Assert.assertEquals(AddressUse.HOME, patient.getAddress().get(0).getUse());
    }

    @Test
    public void getPatientMayJuneMultiLine() throws Exception {
        DiasCprService cprService = new DiasCprService(setupWireMockForCprService());
        Patient patient = cprService.getPatient("0108629996");
        Assert.assertEquals(2, patient.getAddress().get(0).getLine().size());
        Assert.assertEquals("Testgrusgraven 3", patient.getAddress().get(0).getLine().get(0).toString());
        Assert.assertEquals("3.tv", patient.getAddress().get(0).getLine().get(1).toString());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void getPatientWrongIdentifier() throws Exception {
        DiasCprService cprService = new DiasCprService(setupWireMockForCprService());
        Patient patient = cprService.getPatient("KwiehHw12412Gs");
    }

    private String setupWireMockForCprService() {
        stubFor(get(urlEqualTo("/Patient/2512489996")).atPriority(1)
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(nancyFromCprService)));

        stubFor(get(urlEqualTo("/Patient/0108629996")).atPriority(1)
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(mayJuneFromCprService)));

        stubFor(get(urlEqualTo("/Patient/KwiehHw12412Gs")).atPriority(2)
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"patient\": {}}")));

        return "http://localhost:" + wireMockRule.port();
    }

    @BeforeClass
    public static void beforeClass() throws Exception {
        //Read and set environment variables from .env file:
        TestUtils.readEnvironment("service.env", environmentVariables);
        TestUtils.readEnvironment("deploy.env", environmentVariables);
    }
}