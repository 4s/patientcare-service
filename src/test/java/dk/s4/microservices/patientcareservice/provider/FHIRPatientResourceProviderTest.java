package dk.s4.microservices.patientcareservice.provider;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.patientcareservice.CprServiceInterface;
import dk.s4.microservices.patientcareservice.ServiceVariables;
import dk.s4.microservices.patientcareservice.servlet.PatientCareServiceFacade;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.jetty.server.Server;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CarePlan;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Patient;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

public class FHIRPatientResourceProviderTest {
    private static final org.slf4j.Logger ourLog = org.slf4j.LoggerFactory.getLogger(FHIRPatientResourceProviderTest.class);


    private static IFhirResourceDao<CarePlan> carePlanDao;
    private static IFhirResourceDao<Patient> patientDao;
    private static FHIRPatientResourceProvider provider;


    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();
    private static Server ourServer;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(wireMockConfig().dynamicPort());


    @Test(expected = ResourceNotFoundException.class)
    public void testSearchByIdentifierNoneInDao() throws IOException, ParseException {
        Patient patient = new Patient();
        patient.addIdentifier(new Identifier().setSystem("identifierSystem").setValue("identifierValue"));
        patient.getMeta().setLastUpdated(new Date());

        provider = new FHIRPatientResourceProvider(patientDao,
                mock(FhirContext.class),
                carePlanDao);

        Bundle returnBundle = provider.searchByIdentifier(null, null, null,
                new TokenParam().setValue("identifierValue").setSystem("identifierSystem"));
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testSearchByIdentifierInDao() throws IOException, ParseException {
        Patient patient = new Patient();
        String ptIdentifierValue = UUID.randomUUID().toString();
        patient.addIdentifier(new Identifier().setSystem("identifierSystem").setValue(ptIdentifierValue));
        patientDao.create(patient);

        provider = new FHIRPatientResourceProvider(patientDao,
                mock(FhirContext.class),
                carePlanDao);

        Bundle returnBundle = provider.searchByIdentifier(null, null, null,
                new TokenParam().setValue(ptIdentifierValue).setSystem("identifierSystem"));
        Assert.assertTrue(returnBundle.hasEntry());

        Patient returnPatient = (Patient) returnBundle.getEntryFirstRep().getResource();
        Assert.assertNotNull(returnPatient.getId());
        Assert.assertEquals("identifierSystem",returnPatient.getIdentifierFirstRep().getSystem());
        Assert.assertEquals("identifierValue",returnPatient.getIdentifierFirstRep().getValue());
    }

    @BeforeClass
    public static void beforeClass() throws Exception {
        TestUtils.readEnvironment("service.env", new EnvironmentVariables());
        TestUtils.readEnvironment("deploy.env", new EnvironmentVariables());

        environmentVariables.set("DATABASE_TYPE", "Derby");

        //Disable authentication
        environmentVariables.set("ENABLE_AUTHENTICATION", "false");
        environmentVariables.set("ENABLE_DIAS_AUTHENTICATION", "false");

        //Init web server and start it
        int ourPort = TestUtils.findFreePort();
        ourServer = new Server(ourPort);
        TestUtils.initWebServer("src/main/webapp/WEB-INF/without-keycloak/web.xml","target/service", environmentVariables, ourPort, ourServer);


        patientDao = PatientCareServiceFacade.getPatientDao();
        carePlanDao = PatientCareServiceFacade.getCarePlanDao();
    }


    @AfterClass
    public static void afterClass() throws Exception {
        ourServer.stop();
    }
}