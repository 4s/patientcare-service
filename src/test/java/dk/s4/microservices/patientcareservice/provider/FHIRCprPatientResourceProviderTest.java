package dk.s4.microservices.patientcareservice.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.param.TokenParam;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.patientcareservice.CprServiceInterface;
import dk.s4.microservices.patientcareservice.ServiceVariables;
import dk.s4.microservices.patientcareservice.servlet.PatientCareServiceFacade;
import dk.s4.microservices.patientcareservice.servlet.RestfulFHIRProvidersTest;
import org.eclipse.jetty.server.Server;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CarePlan;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Patient;
import org.junit.*;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FHIRCprPatientResourceProviderTest {
    private static final org.slf4j.Logger ourLog = org.slf4j.LoggerFactory.getLogger(FHIRCprPatientResourceProviderTest.class);


    private static IFhirResourceDao<CarePlan> carePlanDao;
    private static IFhirResourceDao<Patient> patientDao;
    private static FHIRCprPatientResourceProvider provider;


    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();
    private static Server ourServer;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(wireMockConfig().dynamicPort());


    @Test
    public void testSearchByIdentifierNoneInDao() throws IOException, ParseException {
        Patient patient = new Patient();
        patient.addIdentifier(new Identifier().setSystem("identifierSystem").setValue("identifierValue"));
        patient.getMeta().setLastUpdated(new Date());

        CprServiceInterface cprServiceInterface = mock(CprServiceInterface.class);
        when(cprServiceInterface.getPatient(anyObject())).thenReturn(patient);

        provider = new FHIRCprPatientResourceProvider(patientDao,
                mock(FhirContext.class),
                carePlanDao,
                cprServiceInterface);

        Bundle returnBundle = provider.searchByIdentifier(null, null, null,
                new TokenParam().setValue("identifierValue").setSystem("identifierSystem"));
        Assert.assertTrue(returnBundle.hasEntry());

        Patient returnPatient = (Patient) returnBundle.getEntryFirstRep().getResource();
        Assert.assertNotNull(returnPatient.getId());
        Assert.assertEquals("identifierSystem",returnPatient.getIdentifierFirstRep().getSystem());
        Assert.assertEquals("identifierValue",returnPatient.getIdentifierFirstRep().getValue());
    }
    @Test
    public void testSearchByIdentifierPatientOutdated() throws IOException, ParseException {
        Patient patient = new Patient();
        patient.addIdentifier(new Identifier().setSystem("identifierSystem").setValue("identifierValue"));
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -2*(Integer.parseInt(ServiceVariables.OUTDATED_IF_OLDER_THAN_DAYS.getValue())));
        patient.getMeta().setLastUpdated(cal.getTime());
        patientDao.create(patient);

        CprServiceInterface cprServiceInterface = mock(CprServiceInterface.class);
        when(cprServiceInterface.getPatient(anyObject())).thenReturn(patient);

        provider = new FHIRCprPatientResourceProvider(patientDao,
                mock(FhirContext.class),
                carePlanDao,
                cprServiceInterface);

        Bundle returnBundle = provider.searchByIdentifier(mock(HttpServletRequest.class),
                mock(HttpServletResponse.class),mock(RequestDetails.class),
                new TokenParam().setValue("identifierValue").setSystem("identifierSystem"));
        Assert.assertTrue(returnBundle.hasEntry());

        Patient returnPatient = (Patient) returnBundle.getEntryFirstRep().getResource();
        Assert.assertNotNull(returnPatient.getId());
        Assert.assertEquals(returnPatient.getIdentifierFirstRep().getSystem(), "identifierSystem");
        Assert.assertEquals(returnPatient.getIdentifierFirstRep().getValue(), "identifierValue");
    }

    @Test
    public void testSearchByIdentifierPatientNotOutdated() throws IOException, ParseException {
        Patient patient = new Patient();
        patient.addIdentifier(new Identifier().setSystem("identifierSystem").setValue("identifierValue"));
        patient.getMeta().setLastUpdated(new Date());
        patientDao.create(patient);

        CprServiceInterface cprServiceInterface = mock(CprServiceInterface.class);
        when(cprServiceInterface.getPatient(anyObject())).thenReturn(patient);

        provider = new FHIRCprPatientResourceProvider(patientDao,
                mock(FhirContext.class),
                carePlanDao,
                cprServiceInterface);

        Bundle returnBundle = provider.searchByIdentifier(mock(HttpServletRequest.class),
                mock(HttpServletResponse.class),mock(RequestDetails.class),
                new TokenParam().setValue("identifierValue").setSystem("identifierSystem"));
        Assert.assertTrue(returnBundle.hasEntry());

        Patient returnPatient = (Patient) returnBundle.getEntryFirstRep().getResource();
        Assert.assertNotNull(returnPatient.getId());
        Assert.assertEquals(returnPatient.getIdentifierFirstRep().getSystem(), "identifierSystem");
        Assert.assertEquals(returnPatient.getIdentifierFirstRep().getValue(), "identifierValue");
    }

    @Test
    public void testPatientIsNotOutdated() {
        ourLog.debug("testPatientIsNotOutdated");

        Patient testPatient = new Patient();
        testPatient.getMeta().setLastUpdated(new Date());

        provider = new FHIRCprPatientResourceProvider(patientDao, mock(FhirContext.class), carePlanDao, mock(CprServiceInterface.class));

        Assert.assertFalse(provider.isPatientOutdated(testPatient));
    }

    @Test
    public void testIsPatientOutdated() throws ParseException {
        ourLog.debug("testIsPatientOutdated");

        Patient testPatient = new Patient();
        String dateString = "2019-04-01";
        DateFormat format = new SimpleDateFormat("yyyy-dd-MM", Locale.ENGLISH);
        Date date = format.parse(dateString);
        testPatient.getMeta().setLastUpdated(date);

        provider = new FHIRCprPatientResourceProvider(patientDao, mock(FhirContext.class), carePlanDao, mock(CprServiceInterface.class));

        Assert.assertTrue(provider.isPatientOutdated(testPatient));
    }

    @BeforeClass
    public static void beforeClass() throws Exception {
        TestUtils.readEnvironment("service.env", new EnvironmentVariables());
        TestUtils.readEnvironment("deploy.env", new EnvironmentVariables());

        environmentVariables.set("DATABASE_TYPE", "Derby");

        //Disable authentication
        environmentVariables.set("ENABLE_AUTHENTICATION", "false");
        environmentVariables.set("ENABLE_DIAS_AUTHENTICATION", "false");

        //Init web server and start it
        int ourPort = TestUtils.findFreePort();
        ourServer = new Server(ourPort);
        TestUtils.initWebServer("src/main/webapp/WEB-INF/without-keycloak/web.xml","target/service", environmentVariables, ourPort, ourServer);


        patientDao = PatientCareServiceFacade.getPatientDao();
        carePlanDao = PatientCareServiceFacade.getCarePlanDao();
    }


    @AfterClass
    public static void afterClass() throws Exception {
        ourServer.stop();
    }
}