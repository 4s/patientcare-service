package dk.s4.microservices.patientcareservice.messaging;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.DaoConfig;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import ca.uhn.fhir.jpa.searchparam.registry.ISearchParamRegistry;
import ca.uhn.fhir.jpa.util.ExpungeOptions;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import dk.s4.microservices.microservicecommon.fhir.SearchParameterFacade;
import dk.s4.microservices.patientcareservice.TestFhirServerConfigR4;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static dk.s4.microservices.microservicecommon.fhir.ResourceUtil.getResourceStream;
import static junit.framework.TestCase.fail;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {TestFhirServerConfigR4.class})
public class ResourceDeleterTest {

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Autowired
    @Qualifier("myPatientDaoR4")
    private IFhirResourceDao<Patient> patientDao;

    @Autowired
    @Qualifier("myCarePlanDaoR4")
    private IFhirResourceDao<CarePlan> carePlanDao;

    @Autowired
    private DaoConfig daoConfig;

    @Autowired
    @Qualifier("mySearchParameterDaoR4")
    private IFhirResourceDao<SearchParameter> searchParameterDaoR4;

    @Autowired
    private ISearchParamRegistry searchParamRegistry;

    private ResourceDeleter<Patient> patientDeleter;
    private ResourceDeleter<CarePlan> carePlanDeleter;

    @Before
    public void before() throws IOException {
        FhirContext fhirContext =  FhirContext.forR4();
        SearchParameterFacade searchParameterFacade = new SearchParameterFacade(searchParameterDaoR4, fhirContext);
        searchParameterFacade.installSearchParameter(getResourceStream("CarePlanSubjectIdentifierSearchParam.json"));
        searchParameterFacade.installSearchParameter(getResourceStream("PatientIdentifierSearchParam.json"));
        searchParamRegistry.forceRefresh();

        environmentVariables.set("OFFICIAL_PATIENT_IDENTIFIER_SYSTEM", "urn:oid:1.2.208.176.1.2");
        this.patientDeleter = new ResourceDeleter<Patient>( patientDao, daoConfig);
        this.carePlanDeleter = new ResourceDeleter<CarePlan>(carePlanDao, daoConfig);

        emptyDb();

    }

    private void emptyDb(){
        boolean expungeEnabled = daoConfig.isExpungeEnabled();
        daoConfig.setExpungeEnabled(true);

        ExpungeOptions expungeOptions = new ExpungeOptions()
                .setExpungeDeletedResources(true)
                .setExpungeOldVersions(true);

        IBundleProvider foundPatients = patientDao.search(new SearchParameterMap());
        for(IBaseResource base : foundPatients.getResources(0, foundPatients.size())){
            patientDao.delete(base.getIdElement());
            patientDao.expunge(base.getIdElement(), expungeOptions);
        }

        IBundleProvider foundCarePlans = carePlanDao.search(new SearchParameterMap());
        for(IBaseResource base : foundCarePlans.getResources(0, foundCarePlans.size())){
            carePlanDao.delete(base.getIdElement());
            carePlanDao.expunge(base.getIdElement(), expungeOptions);
        }

        daoConfig.setExpungeEnabled(expungeEnabled);
    }

    @Test
    public void removeConsentFromDbPatient() throws Exception{

        String deleteId = "3a3a43bf-507c-46b5-a265-c98c07bfb908";

        patientDao.create(createPatient(deleteId));
        IIdType createdId = patientDao.create(createPatient(deleteId)).getId();
        patientDao.create(createPatient("3a3a43bf-507c-46b5-a265-c98c07bfb910"));
        patientDao.create(createPatient("3a3a43bf-507c-46b5-a265-c98c07bfb911"));
        patientDao.create(createPatient("3a3a43bf-507c-46b5-a265-c98c07bfb912"));

        try{
            patientDao.read(createdId);
        }
        catch (Exception e){
            fail();
        }

        Future<?> future = this.patientDeleter.deleteResourceByUserIdentifier(deleteId);
        future.get(1000, TimeUnit.MILLISECONDS);

        IBundleProvider found = patientDao.search(new SearchParameterMap());
        Assert.assertEquals(3, found.size().intValue());

        for(IBaseResource base : found.getResources(0, found.size())){
            Patient o = (Patient) base;
            Assert.assertNotEquals(o.getIdentifier().get(0).getValue(), deleteId);
        }

        try{
            patientDao.read(createdId);
            fail();
        }
        catch (Exception e){
            // good
        }
    }

    @Test
    public void removeConsentEmptyDbPatient() throws Exception{
        String deleteId = "3a3a43bf-507c-46b5-a265-c98c07bfb908";
        Future<?> future = this.patientDeleter.deleteResourceByUserIdentifier(deleteId);
        future.get(1000, TimeUnit.MILLISECONDS);
        IBundleProvider found = patientDao.search(new SearchParameterMap());
        Assert.assertEquals(0, found.size().intValue());
    }


    @Test
    public void removeConsentNotInDbPatient() throws Exception{

        String deleteId = "3a3a43bf-507c-46b5-a265-c98c07bfb908";

        patientDao.create(createPatient("3a3a43bf-507c-46b5-a265-c98c07bfb910"));
        patientDao.create(createPatient("3a3a43bf-507c-46b5-a265-c98c07bfb911"));
        patientDao.create(createPatient("3a3a43bf-507c-46b5-a265-c98c07bfb912"));

        Future<?> future = this.patientDeleter.deleteResourceByUserIdentifier(deleteId);
        future.get(1000, TimeUnit.MILLISECONDS);

        IBundleProvider found = patientDao.search(new SearchParameterMap());

        Assert.assertEquals(3, found.size().intValue());

        for(IBaseResource base : found.getResources(0, found.size())){
            Patient o = (Patient) base;
            Assert.assertNotEquals(o.getIdentifier().get(0).getValue(), deleteId);
        }
    }

    @Test
    public void removeConsentWithWrongSystemPatient() throws Exception{

        String deleteId = "3a3a43bf-507c-46b5-a265-c98c07bfb910";
        patientDao.create(createPatient("3a3a43bf-507c-46b5-a265-c98c07bfb910", "invalidSystem"));
        patientDao.create(createPatient("3a3a43bf-507c-46b5-a265-c98c07bfb911"));
        patientDao.create(createPatient("3a3a43bf-507c-46b5-a265-c98c07bfb912"));

        Future<?> future = this.patientDeleter.deleteResourceByUserIdentifier(deleteId);
        future.get(1000, TimeUnit.MILLISECONDS);

        IBundleProvider found = patientDao.search(new SearchParameterMap());

        Assert.assertEquals(3, found.size().intValue());

    }

    @Test
    public void removeConsentFromDbCarePlan() throws Exception{

        String deleteId = "3a3a43bf-507c-46b5-a265-c98c07bfb908";

        carePlanDao.create(createCarePlan(deleteId));
        IIdType createdId = carePlanDao.create(createCarePlan(deleteId)).getId();
        carePlanDao.create(createCarePlan("3a3a43bf-507c-46b5-a265-c98c07bfb910"));
        carePlanDao.create(createCarePlan("3a3a43bf-507c-46b5-a265-c98c07bfb911"));
        carePlanDao.create(createCarePlan("3a3a43bf-507c-46b5-a265-c98c07bfb912"));

        try{
            carePlanDao.read(createdId);
        }
        catch (Exception e){
            fail();
        }

        Future<?> future = this.carePlanDeleter.deleteResourceByUserIdentifier(deleteId);
        future.get(1000, TimeUnit.MILLISECONDS);

        IBundleProvider found = carePlanDao.search(new SearchParameterMap());
        Assert.assertEquals(3, found.size().intValue());

        for(IBaseResource base : found.getResources(0, found.size())){
            CarePlan carePlan = (CarePlan) base;
            Assert.assertNotEquals(carePlan.getSubject().getIdentifier().getValue(), deleteId);
        }

        try{
            carePlanDao.read(createdId);
            fail();
        }
        catch (Exception e){
            // good
        }
    }

    @Test
    public void removeConsentEmptyDbCarePlan() throws Exception{
        String deleteId = "3a3a43bf-507c-46b5-a265-c98c07bfb908";
        Future<?> future = this.carePlanDeleter.deleteResourceByUserIdentifier(deleteId);
        future.get(1000, TimeUnit.MILLISECONDS);
        IBundleProvider found = carePlanDao.search(new SearchParameterMap());
        Assert.assertEquals(0, found.size().intValue());
    }


    @Test
    public void removeConsentNotInDbCarePlan() throws Exception{

        String deleteId = "3a3a43bf-507c-46b5-a265-c98c07bfb908";

        carePlanDao.create(createCarePlan("3a3a43bf-507c-46b5-a265-c98c07bfb910"));
        carePlanDao.create(createCarePlan("3a3a43bf-507c-46b5-a265-c98c07bfb911"));
        carePlanDao.create(createCarePlan("3a3a43bf-507c-46b5-a265-c98c07bfb912"));

        Future<?> future = this.carePlanDeleter.deleteResourceByUserIdentifier(deleteId);
        future.get(1000, TimeUnit.MILLISECONDS);

        IBundleProvider found = carePlanDao.search(new SearchParameterMap());

        Assert.assertEquals(3, found.size().intValue());

        for(IBaseResource base : found.getResources(0, found.size())){
            CarePlan carePlan = (CarePlan) base;
            Assert.assertNotEquals(carePlan.getSubject().getIdentifier().getValue(), deleteId);
        }
    }

    @Test
    public void removeConsentWithWrongSystemCarePlan() throws Exception{

        String deleteId = "3a3a43bf-507c-46b5-a265-c98c07bfb910";
        carePlanDao.create(createCarePlan("3a3a43bf-507c-46b5-a265-c98c07bfb910", "invalidSystem"));
        carePlanDao.create(createCarePlan("3a3a43bf-507c-46b5-a265-c98c07bfb911"));
        carePlanDao.create(createCarePlan("3a3a43bf-507c-46b5-a265-c98c07bfb912"));

        Future<?> future = this.carePlanDeleter.deleteResourceByUserIdentifier(deleteId);
        future.get(1000, TimeUnit.MILLISECONDS);

        IBundleProvider found = carePlanDao.search(new SearchParameterMap());

        Assert.assertEquals(3, found.size().intValue());

    }

    private Patient createPatient(String id){
        return createPatient(id, System.getenv("OFFICIAL_PATIENT_IDENTIFIER_SYSTEM"));
    }

    private Patient createPatient(String id, String system){
        return new Patient().addIdentifier(new Identifier().setSystem(system).setValue(id));
    }

    private CarePlan createCarePlan(String id, String system){
        CarePlan carePlan = new CarePlan();
        carePlan.setSubject(new Reference().setIdentifier(new Identifier().setSystem(system)
                .setValue(id)));
        return carePlan;
    }

    private CarePlan createCarePlan(String id){
        return createCarePlan(id, System.getenv("OFFICIAL_PATIENT_IDENTIFIER_SYSTEM"));
    }

}
