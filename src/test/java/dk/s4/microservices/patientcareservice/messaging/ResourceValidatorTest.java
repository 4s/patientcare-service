package dk.s4.microservices.patientcareservice.messaging;

import ca.uhn.fhir.rest.server.exceptions.UnprocessableEntityException;
import dk.s4.microservices.patientcareservice.ServiceVariables;
import dk.s4.microservices.patientcareservice.servlet.RestfulFHIRProvidersTest;
import org.hl7.fhir.r4.model.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ResourceValidatorTest {

    private static final org.slf4j.Logger ourLog = org.slf4j.LoggerFactory.getLogger(RestfulFHIRProvidersTest.class);

    @Before
    public void setup() {
        EnvironmentVariables variables = new EnvironmentVariables();
        variables.set("OFFICIAL_THRESHOLDSET_APPLIESTO_URL", "https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100728906/ThresholdSet+appliesTo+extension");
    }

    private Basic createTestThresholdSet(String canonicalString) {
        Basic thresholdSet = mock(Basic.class);
        Extension extension = mock(Extension.class);
        CanonicalType canonicalType = mock(CanonicalType.class);

        when(thresholdSet.getExtensionByUrl(anyObject())).thenReturn(extension);
        when(canonicalType.getValue()).thenReturn(canonicalString);
        when(extension.getValue()).thenReturn(canonicalType);

        return thresholdSet;
    }


    @Test
    public void testExtractIdentifiersSameValueDifferentVersion() {
        ourLog.debug("testExtractIdentifiersSameValueDifferentVersion");
        CarePlan carePlan = new CarePlan();
        carePlan.addContained(createTestThresholdSet("something|1.0.0"))
                .addContained(createTestThresholdSet("something-else|1.0.0"));

        Assert.assertTrue(ResourceValidator.validateContainedThresholdSets(carePlan));
    }

    @Test
    public void testExtractIdentifiersSameValueDifferentVersionWithCollision() {
        ourLog.debug("testExtractIdentifiersSameValueDifferentVersion");
        CarePlan carePlan = new CarePlan();
        carePlan.addContained(createTestThresholdSet("something|1.0.0"))
                .addContained(createTestThresholdSet("something|2.0.0"));

        Assert.assertFalse(ResourceValidator.validateContainedThresholdSets(carePlan));
    }

    @Test
    public void testExtractCanonicals() {
        List<Extension> extensions = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Extension extension = mock(Extension.class);
            CanonicalType canonicalType = mock(CanonicalType.class);
            when(canonicalType.getValue()).thenReturn("canonical" + i + "|1.0.0");
            when(extension.getValue()).thenReturn(canonicalType);

            extensions.add(extension);
        }

        List<String> canonicals = ResourceValidator.extractCanonicals(extensions);

        Assert.assertEquals(10, canonicals.size());
    }

    @Test
    public void testExtractCanonicalsVersionIgnored() {
        List<Extension> extensions = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Extension extension = mock(Extension.class);
            CanonicalType canonicalType = mock(CanonicalType.class);
            when(canonicalType.getValue()).thenReturn("canonical|" + i + ".0.0");
            when(extension.getValue()).thenReturn(canonicalType);

            extensions.add(extension);
        }

        List<String> canonicals = ResourceValidator.extractCanonicals(extensions);

        for (String canonical : canonicals) {
            Assert.assertFalse(canonical.contains("|"));
            Assert.assertFalse(canonical.contains(".0.0"));
        }
    }

    @Test
    public void testExcludeVersion() {
        String string = "abc|1234";
        String resultString = ResourceValidator.excludeVersion(string);
        Assert.assertFalse(resultString.contains("1234"));
        Assert.assertFalse(resultString.contains("|"));
    }

    @Test
    public void testExcludeVersionNoPipe() {
        String string = "abc";
        String resultString = ResourceValidator.excludeVersion(string);
        Assert.assertTrue(resultString.contains("abc"));
    }

    @Test
    public void testExtractExtensionsFromBasic() {
        ourLog.debug("testExtractIdentifiersSameValueDifferentVersion");

        List<Resource> resources = new ArrayList<>();

        Basic thresholdSet = mock(Basic.class);
        Extension extension = mock(Extension.class);
        CanonicalType canonicalType = mock(CanonicalType.class);

        when(thresholdSet.getExtensionByUrl(ServiceVariables.OFFICIAL_THRESHOLDSET_APPLIESTO_URL.getValue())).thenReturn(extension);
        when(extension.getValue()).thenReturn(canonicalType);
        when(canonicalType.getValue()).thenReturn("identifier|1.0.0");

        resources.add(thresholdSet);

        List<Extension> extensions = ResourceValidator.extractExtensionsFromBasic(resources, ServiceVariables.OFFICIAL_THRESHOLDSET_APPLIESTO_URL.getValue());

        Assert.assertFalse(extensions.isEmpty());
    }

    @Test(expected = UnprocessableEntityException.class)
    public void testExtractExtensionsFromBasicInvalidUrl() {
        ourLog.debug("testExtractIdentifiersSameValueDifferentVersion");

        List<Resource> resources = new ArrayList<>();

        Basic thresholdSet = mock(Basic.class);
        Extension extension = mock(Extension.class);
        CanonicalType canonicalType = mock(CanonicalType.class);

        when(extension.getValue()).thenReturn(canonicalType);
        when(canonicalType.getValue()).thenReturn("identifier|1.0.0");

        resources.add(thresholdSet);

        List<Extension> extensions = ResourceValidator.extractExtensionsFromBasic(resources, ServiceVariables.OFFICIAL_THRESHOLDSET_APPLIESTO_URL.getValue());
    }

    @Test
    public void testExtractExtensionsFromBasicNoBasic() {
        ourLog.debug("testExtractIdentifiersSameValueDifferentVersion");

        List<Resource> resources = new ArrayList<>();

        resources.add(new CarePlan());

        List<Extension> extensions = ResourceValidator.extractExtensionsFromBasic(resources, ServiceVariables.OFFICIAL_THRESHOLDSET_APPLIESTO_URL.getValue());

        Assert.assertTrue(extensions.isEmpty());
    }

    @Test
    public void testThereAreCollision() {
        List<String> canonicals = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            canonicals.add("value for " + i);
        }

        Assert.assertFalse(ResourceValidator.thereAreCollisions(canonicals));

        canonicals.add("value for 1");

        Assert.assertTrue(ResourceValidator.thereAreCollisions(canonicals));
    }
}