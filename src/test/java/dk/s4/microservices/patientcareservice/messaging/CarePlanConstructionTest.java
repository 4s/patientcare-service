package dk.s4.microservices.patientcareservice.messaging;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.patientcareservice.CprServiceInterface;
import dk.s4.microservices.patientcareservice.ServiceVariables;
import dk.s4.microservices.patientcareservice.provider.FHIRCprPatientResourceProvider;
import dk.s4.microservices.patientcareservice.provider.FHIRPatientResourceProvider;
import dk.s4.microservices.patientcareservice.servlet.RestfulFHIRProvidersTest;
import org.hl7.fhir.r4.model.CarePlan;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Reference;
import org.junit.*;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static dk.s4.microservices.patientcareservice.messaging.MyEventProcessor.addDateToCarePlan;
import static org.mockito.Mockito.mock;

public class CarePlanConstructionTest {

    private static final org.slf4j.Logger ourLog = org.slf4j.LoggerFactory.getLogger(RestfulFHIRProvidersTest.class);

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(wireMockConfig().dynamicPort());

    @Test
    public void testAddDateToCarePlan() {
        ourLog.debug("testAddDateToCarePlan");
        CarePlan carePlan = new CarePlan();
        carePlan = addDateToCarePlan(carePlan);
        Assert.assertTrue(carePlan.getPeriod().hasEnd());
    }

    @Test
    public void testConstructParamMapForCarePlan() {
        ourLog.debug("testConstructParamMapForCarePlan");

        CarePlan carePlan = new CarePlan();

        Reference subjectReference = new Reference();
        Identifier subjectIdentifier = new Identifier();
        subjectIdentifier.setValue("123");
        subjectIdentifier.setSystem("urn:oid:1.2.208.176.1.2");
        subjectReference.setIdentifier(subjectIdentifier);


        Identifier carePlanIdentifier = new Identifier();
        carePlanIdentifier.setValue("411");
        carePlanIdentifier.setSystem("urn:ietf:rfc:3986");
        Reference authorReference = new Reference();
        authorReference.setIdentifier(carePlanIdentifier);

        carePlan.setAuthor(authorReference);
        carePlan.setSubject(subjectReference);
        carePlan.setStatus(CarePlan.CarePlanStatus.valueOf("ACTIVE"));


        SearchParameterMap parameterMap = MyEventProcessor.constructParamMapForCarePlan(carePlan);

        Assert.assertEquals("[[TokenParam[system=urn:oid:1.2.208.176.1.2,value=123]]]", parameterMap.get("subjectIdentifier").toString());

        Assert.assertEquals("[[TokenParam[system=urn:ietf:rfc:3986,value=411]]]", parameterMap.get("authorIdentifier").toString());
        Assert.assertEquals("[[TokenParam[system=,value=ACTIVE]]]", parameterMap.get("status").toString());

    }

    @Test
    public void testCreateBrugsForCarePlan() throws IOException {
        ourLog.debug("testCreateBrugsForCarePlan");

        setupWireMockForBrugsAftale();

        String retString = MyEventProcessor.createBrugsForCarePlan("2512489996");
        Assert.assertNotEquals(retString, "Error");

    }

    private void setupWireMockForBrugsAftale() {
        String url = "http://localhost:" + wireMockRule.port() + "/";
        environmentVariables.set("BRUGS_SERVICE_URL", url);


        stubFor(post(urlEqualTo("/api/2512489996"))
                .willReturn(aResponse()
                        .withStatus(201)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"resourceType\": \"Brugsaftale\",\"id\": \"42\"}")));
    }


    @BeforeClass
    public static void beforeClass() throws Exception {
        IFhirResourceDao<Patient> patientDao = (IFhirResourceDao<Patient>) mock(IFhirResourceDao.class);

        IFhirResourceDao<CarePlan> carePlanDao = (IFhirResourceDao<CarePlan>) mock(IFhirResourceDao.class);
        FHIRPatientResourceProvider provider = new FHIRCprPatientResourceProvider(patientDao,
                mock(FhirContext.class),
                carePlanDao,
                mock(CprServiceInterface.class));

        //Read and set environment variables from .env file:
        TestUtils.readEnvironment("service.env", environmentVariables);
        TestUtils.readEnvironment("deploy.env", environmentVariables);

        String value = ServiceVariables.DATABASE_TYPE.getValue();
        environmentVariables.set("DATABASE_TYPE", "Derby");

        //Disable authentication
        environmentVariables.set("ENABLE_AUTHENTICATION", "false");
        environmentVariables.set("ENABLE_DIAS_AUTHENTICATION", "false");
    }
}
