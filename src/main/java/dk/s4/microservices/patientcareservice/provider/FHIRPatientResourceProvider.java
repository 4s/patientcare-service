package dk.s4.microservices.patientcareservice.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.DaoMethodOutcome;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.provider.BaseJpaResourceProvider;
import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.Patch;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.RequiredParam;
import ca.uhn.fhir.rest.annotation.ResourceParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.api.PatchTypeEnum;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.param.TokenOrListParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.exceptions.NotImplementedOperationException;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import dk.s4.microservices.patientcareservice.ServiceVariables;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.CarePlan;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Resource provider for the FHIR Patient resource.
 */
public class FHIRPatientResourceProvider extends BaseJpaResourceProvider<Patient> {

    private static final Logger logger = LoggerFactory.getLogger(FHIRPatientResourceProvider.class);
    private IFhirResourceDao<CarePlan> carePlanDao;

    public FHIRPatientResourceProvider(IFhirResourceDao<Patient> dao, FhirContext fhirContext,
            IFhirResourceDao<CarePlan> carePlanDao) {
        super(dao);
        setContext(fhirContext);
        this.carePlanDao = carePlanDao;
    }

    /**
     * Returns the type of resource this provider provides.
     */
    @Override
    public Class<Patient> getResourceType() {
        return Patient.class;
    }

    /**
     * We don't want to support read operation via REST. We thus override read defined in BaseJpaResourceProvider and
     * always throw a NotImplementedOperationException.
     */
    @Override
    @Read
    public Patient read(HttpServletRequest theRequest, @IdParam IIdType theId, RequestDetails theRequestDetails) {
        throw new NotImplementedOperationException("RESTful read operation is not supported by this FHIR server");
    }

    /**
     * We don't want to support patch operation via REST. We thus override patch defined in BaseJpaResourceProvider and
     * always throw a NotImplementedOperationException.
     */
    @Override
    @Patch
    public DaoMethodOutcome patch(HttpServletRequest theRequest, @IdParam IIdType theId,
                                  RequestDetails theRequestDetails, @ResourceParam String theBody, PatchTypeEnum thePatchType) {
        throw new NotImplementedOperationException("RESTful patch operation is not supported by this FHIR server");
    }

    /**
     * Search for patient by Identifier.
     *
     * @param theRequest the request
     * @param theResponse the response
     * @param identifier  The identifier number of the patient.
     * @return The found patient resource (returned in a FHIR Bundle)
     */
    @Search(queryName = "searchByIdentifier")
    public Bundle searchByIdentifier(HttpServletRequest theRequest, HttpServletResponse theResponse, RequestDetails theRequestDetails,
                                     @RequiredParam(name = "identifier") TokenParam identifier) throws ParseException, IOException {

        // Search database for requested identifier
        IBundleProvider searchResult = findPatientResource(identifier.getValue(), theResponse, theRequestDetails);
        Bundle result = new Bundle();
        if (searchResult.size() > 0) {
            result.addEntry(new BundleEntryComponent().setResource((Resource) searchResult.getResources(0,1).get(0)));
        } else {
            throw new ResourceNotFoundException("A resource with the given identifier was not found.");
        }

        return result;
    }

    /**
     * Returns every patient connected to given organization though a CarePlan.
     * @param theRequest the request.
     * @param theResponse the response.
     * @param theRequestDetails the request details.
     * @param authorIdentifier the identifier of the organization.
     */
    @Search(queryName = "getConnectedToOrganization")
    public IBundleProvider getConnectedToOrganization(HttpServletRequest theRequest, HttpServletResponse theResponse, RequestDetails theRequestDetails,
                           @RequiredParam(name = "authorIdentifier") TokenOrListParam authorIdentifier) {
        try{
            SearchParameterMap parameterMap = new SearchParameterMap();
            parameterMap.add("authorIdentifier", authorIdentifier);
            IBundleProvider carePlanResults = carePlanDao.search(parameterMap);

            if(carePlanResults.size() == 0){
                throw new ResourceNotFoundException("There exists no CarePlans belonging to the given organizations");
            }

            List<IBaseResource> careplans = carePlanResults.getResources(0,carePlanResults.size());
            SearchParameterMap patientIdentifierParameterMap = new SearchParameterMap();
            TokenOrListParam orListParam = new TokenOrListParam();
            for (IBaseResource resource : careplans){
                CarePlan carePlan = (CarePlan) resource;
                TokenParam tokenParam = new TokenParam()
                        .setValue(carePlan.getSubject().getIdentifier().getValue())
                        .setSystem(ServiceVariables.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.getValue());
                orListParam.add(tokenParam);
            }
            patientIdentifierParameterMap.add(Patient.SP_IDENTIFIER, orListParam);

            return getDao().search(patientIdentifierParameterMap);
        } finally {
            endRequest(theRequest);
        }
    }

    /**
     * Retrieves a Patient resource from the patientDao
     *
     * @param patientIdentifier patient identifier
     * @param theResponse the response
     * @param theRequestDetails the details
     * @return result of search
     */
    protected IBundleProvider findPatientResource(String patientIdentifier, HttpServletResponse theResponse, RequestDetails theRequestDetails) {
        TokenParam identifier = new TokenParam(ServiceVariables.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.getValue(), patientIdentifier);
        SearchParameterMap paramMap = new SearchParameterMap();
        paramMap.add(Patient.SP_IDENTIFIER, identifier);
        return getDao().search(paramMap, theRequestDetails, theResponse);
    }
}
