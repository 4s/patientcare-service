package dk.s4.microservices.patientcareservice.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.DaoMethodOutcome;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.provider.BaseJpaResourceProvider;
import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import ca.uhn.fhir.model.api.Include;
import ca.uhn.fhir.model.api.annotation.Description;
import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.PatchTypeEnum;
import ca.uhn.fhir.rest.api.SearchTotalModeEnum;
import ca.uhn.fhir.rest.api.SortSpec;
import ca.uhn.fhir.rest.api.SummaryEnum;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.client.exceptions.FhirClientConnectionException;
import ca.uhn.fhir.rest.param.*;
import ca.uhn.fhir.rest.server.exceptions.NotImplementedOperationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dk.s4.microservices.microservicecommon.fhir.ExceptionUtils;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import dk.s4.microservices.patientcareservice.ServiceVariables;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CarePlan;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Patient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Class that represents a resource provider for the FHIR CarePlan resource.
 */
public class FHIRCarePlanResourceProvider extends BaseJpaResourceProvider<CarePlan> {

    private static final Logger logger = LoggerFactory.getLogger(FHIRCarePlanResourceProvider.class);


    public FHIRCarePlanResourceProvider(IFhirResourceDao<CarePlan> dao,
                                        FhirContext fhirContext) {
        super(dao);
        setContext(fhirContext);
    }

    /**
     * Returns the type of resource this provider provides.
     */
    @Override
    public Class<CarePlan> getResourceType() {
        return CarePlan.class;
    }

    /**
     * We don't want to support read operation via REST. We thus override read defined in BaseJpaResourceProvider and
     * always throw a NotImplementedOperationException.
     */
    @Override
    @Read
    public CarePlan read(HttpServletRequest theRequest, @IdParam IIdType theId, RequestDetails theRequestDetails) {
        throw new NotImplementedOperationException("RESTful read operation is not supported by this FHIR server");
    }

    /**
     * We don't want to support patch operation via REST. We thus override patch defined in BaseJpaResourceProvider and
     * always throw a NotImplementedOperationException.
     */
    @Override
    @Patch
    public DaoMethodOutcome patch(HttpServletRequest theRequest, @IdParam IIdType theId,
                                  RequestDetails theRequestDetails, @ResourceParam String theBody, PatchTypeEnum thePatchType) {
        throw new NotImplementedOperationException("RESTful patch operation is not supported by this FHIR server");
    }

    /**
     * Generic search API for retrieval of Bundles of CarePlans stored by this service
     * @param theServletRequest the request
     * @param theServletResponse the response
     * @param theRequestDetails the details
     * @param the_id The ID of the resource
     * @param theIdentifier the Identifier of this plan
     * @param theInstantiates_canonical Instantiates FHIR protocol or definition
     * @param theIntent proposal | plan | order | option
     * @param theStatus draft | active | suspended | completed | entered-in-error | cancelled | unknown
     * @param theLastUpdated Only return resources which were last updated as specified by the given range
     * @param theSort sort order
     * @param theCount count
     * @param theSummaryMode summary mode
     * @param theSearchTotalMode search total mode
     * @return the search results
     */

    @Search()
    public IBundleProvider search(
            javax.servlet.http.HttpServletRequest theServletRequest,
            javax.servlet.http.HttpServletResponse theServletResponse,
            ca.uhn.fhir.rest.api.server.RequestDetails theRequestDetails,
            @Description(shortDefinition="the ID of the resource")
            @OptionalParam(name="_id") TokenAndListParam the_id,
            @Description(shortDefinition="the Identifier of this plan")
            @OptionalParam(name="identifier") TokenAndListParam theIdentifier,
            @Description(shortDefinition="Instantiates FHIR protocol or definition")
            @OptionalParam(name="instantiates-canonical", targetTypes={  } ) ReferenceAndListParam theInstantiates_canonical,
            @Description(shortDefinition="proposal | plan | order | option")
            @OptionalParam(name="intent") TokenAndListParam theIntent,
            @Description(shortDefinition="draft | active | suspended | completed | entered-in-error | cancelled | unknown")
            @OptionalParam(name="status") TokenAndListParam theStatus,
            @Description(shortDefinition="Only return resources which were last updated as specified by the given range")
            @OptionalParam(name="_lastUpdated") DateRangeParam theLastUpdated,
            @Description(shortDefinition="Which organization does this care plan belong to")
            @OptionalParam(name = "subjectIdentifier") TokenParam subjectIdentifier,
            @Description(shortDefinition="Who the care plan is for")
            @OptionalParam(name = "authorIdentifier") TokenOrListParam authorIdentifier,
            @Sort SortSpec theSort,
            @Count Integer theCount,
            SummaryEnum theSummaryMode,
            SearchTotalModeEnum theSearchTotalMode

    ) {
        startRequest(theServletRequest);
        try {
            SearchParameterMap paramMap = new SearchParameterMap();
            paramMap.add("_id", the_id);
            paramMap.add("identifier", theIdentifier);
            paramMap.add("instantiates-canonical", theInstantiates_canonical);
            paramMap.add("intent", theIntent);
            paramMap.add("status", theStatus);
            paramMap.add("subjectIdentifier", subjectIdentifier);
            paramMap.add("authorIdentifier", authorIdentifier);
            paramMap.setLastUpdated(theLastUpdated);
            paramMap.setSort(theSort);
            paramMap.setCount(theCount);
            paramMap.setSummaryMode(theSummaryMode);
            paramMap.setSearchTotalMode(theSearchTotalMode);

            return getDao().search(paramMap, theRequestDetails, theServletResponse);
        } finally {
            endRequest(theServletRequest);
        }
    }


    /**
     * Returns Bundle of active FHIR CarePlan resources, with a subject corresponding to the given CPR parameter.
     *
     * @param theRequest the request
     * @param theResponse the response
     * @param identifier  identifier value of the patient
     * @param organization  identifier for the organization that authored the careplan
     * @return IBundleProvider with the active careplan of the patient
     */
    @Search(queryName = "getActiveForPatient")
    public IBundleProvider getActiveForPatient(HttpServletRequest theRequest,
                                               HttpServletResponse theResponse,
                                               @RequiredParam(name = Patient.SP_IDENTIFIER) TokenParam identifier,
                                               @OptionalParam(name = "author") TokenOrListParam organization) {
        logger.debug("getActiveForPatient");

        this.startRequest(theRequest);
        try {
            SearchParameterMap paramMap = new SearchParameterMap();
            TokenParam activeStatus = new TokenParam().setValue("active");

            // Search for CarePlans with same subject, author, and which is active
            paramMap.add("subjectIdentifier", identifier);
            paramMap.add(CarePlan.SP_STATUS, activeStatus);
            if(organization != null){
                paramMap.add("authorIdentifier", organization);
            }

            return getDao().search(paramMap);

        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        } finally {
            this.endRequest(theRequest);
        }
    }

    /**
     * Returns Bundle of active FHIR CarePlan resources, with a subject corresponding to the given CPR parameter.
     *
     * @param theRequest the request
     * @param theResponse the response
     * @param identifier         identifier value of the patient
     * @param organization  identifier for the organization that authored the careplan
     * @return Bundleprovider with all careplans of the patient
     */
    @Search(queryName = "getAllForPatient")
    public IBundleProvider getAllForPatient(HttpServletRequest theRequest,
                                               HttpServletResponse theResponse,
                                               @RequiredParam(name = Patient.SP_IDENTIFIER) TokenParam identifier,
                                                @OptionalParam(name = "author") TokenOrListParam organization) {
        logger.debug("getAllForPatient");

        this.startRequest(theRequest);
        try {
            SearchParameterMap paramMap = new SearchParameterMap();

            // Search for CarePlans with same subject, author, and which is active
            paramMap.add("subjectIdentifier", identifier);
            if(organization != null){
                paramMap.add("authorIdentifier", organization);
            }

            return getDao().search(paramMap);

        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        } finally {
            this.endRequest(theRequest);
        }
    }
}
