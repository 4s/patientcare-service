package dk.s4.microservices.patientcareservice.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.DaoMethodOutcome;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.rest.annotation.RequiredParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.param.TokenParam;
import dk.s4.microservices.patientcareservice.CprServiceInterface;
import dk.s4.microservices.patientcareservice.ServiceVariables;
import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CarePlan;
import org.hl7.fhir.r4.model.Patient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Resource provider for the FHIR Patient resource. Includes functionality that integrates with external CPR service.
 */
public class FHIRCprPatientResourceProvider extends FHIRPatientResourceProvider {

    private static final Logger logger = LoggerFactory.getLogger(FHIRCprPatientResourceProvider.class);
    private CprServiceInterface cprService;

    public FHIRCprPatientResourceProvider(IFhirResourceDao<Patient> dao, FhirContext fhirContext,
            IFhirResourceDao<CarePlan> carePlanDao,
            CprServiceInterface cprService) {
        super(dao, fhirContext, carePlanDao);
        this.cprService = cprService;
    }

    /**
     * Search for patient by Identifier. Attempts to first retrieve it from the service local storage. If there is no patient that match the argument given,
     * the patient is retrieved from the CPR-service.
     * If collected from local storage, will check when the patient information was last updated.
     * If the patient information was not recently updated (according to system variable OUTDATED_IF_OLDER_THAN_DAYS), fresh patient information will be collected from the CPR-service
     *
     * @param theRequest the request
     * @param theResponse the response
     * @param identifier  The identifier number of the patient.
     * @return The found patient resource (returned in a FHIR Bundle)
     */
    @Override
    @Search(queryName = "searchByIdentifier")
    public Bundle searchByIdentifier(HttpServletRequest theRequest, HttpServletResponse theResponse, RequestDetails theRequestDetails,
                                     @RequiredParam(name = "identifier") TokenParam identifier) throws ParseException, IOException {

        Patient patientResource = null;

        // Search database for requested identifier
        IBundleProvider searchResult = findPatientResource(identifier.getValue(), theResponse, theRequestDetails);
        if (searchResult.size() > 0) {
            // Get first search result
            Patient patientFromDao = (Patient) searchResult.getResources(0, searchResult.size()).get(searchResult.size()-1);

            // If patient have not been recently updated, collect patient from CPR-service, and update in dao
            if (isPatientOutdated(patientFromDao)) {
                patientResource = cprService.getPatient(identifier.getValue());

                patientResource = (Patient) getDao().update(updatePatient(patientFromDao, patientResource)).getResource();
            } else {
                // If patient have been recently updated, return it.
                patientResource = patientFromDao;
            }
        } else {
            // If patientDao did not contain a patient matching the search criteria, we look it up in the CPR-service
            patientResource = cprService.getPatient(identifier.getValue());
            // Create the resource in dao
            DaoMethodOutcome outcome = getDao().create(patientResource);
            //Return the resource that is in the dao
            patientResource = getDao().read(outcome.getId());
        }

        Bundle result = new Bundle();
        result.addEntry(new Bundle.BundleEntryComponent().setResource(patientResource));

        return result;
    }

    /**
     * Updates the fields of the old patient resource, with the values from the new patient resource
     *
     * @param oldPatient the old patient
     * @param newPatient the new patient
     * @return returns the old patient with updated values
     */
    private Patient updatePatient(Patient oldPatient, Patient newPatient) {
        oldPatient.setAddress(newPatient.getAddress());
        oldPatient.setDeceased(newPatient.getDeceased());
        oldPatient.setName(newPatient.getName());
        oldPatient.setGender(newPatient.getGender());
        return oldPatient;
    }

    /**
     * Checks whether the information in the patient is outdated
     *
     * @param patient the patient
     * @return true if patient is outdated
     */
    protected boolean isPatientOutdated(Patient patient) {
        Date lastUpdated = patient.getMeta().getLastUpdated();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -(Integer.parseInt(ServiceVariables.OUTDATED_IF_OLDER_THAN_DAYS.getValue())));
        Date updateIfBefore = cal.getTime();

        return lastUpdated.before(updateIfBefore);
    }
}
