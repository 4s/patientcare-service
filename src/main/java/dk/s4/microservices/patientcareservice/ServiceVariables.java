package dk.s4.microservices.patientcareservice;

import dk.s4.microservices.microservicecommon.Env;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Enumeration of all environment variables, this service uses. Some variables are required while others are
 * optional.
 * This class ensures consistent behavior when accessing these variables, and convenience for validation and
 * access.
 */
public enum ServiceVariables {
    SERVICE_NAME("SERVICE_NAME", true),
    SERVER_BASE_URL("SERVER_BASE_URL",true),
    FHIR_VERSION("FHIR_VERSION", true),
    CORRELATION_ID("CORRELATION_ID", true),
    TRANSACTION_ID("TRANSACTION_ID", true),
    ENABLE_KAFKA("ENABLE_KAFKA",true),
    KEYCLOAK_CLIENT_NAME("KEYCLOAK_CLIENT_NAME", false),
    ENABLE_AUTHENTICATION("ENABLE_AUTHENTICATION", false),
    ENABLE_DIAS_AUTHENTICATION("ENABLE_DIAS_AUTHENTICATION", true),
    ENABLE_KEYCLOAK_GATEKEEPER_AUTHORIZATION("ENABLE_KEYCLOAK_GATEKEEPER_AUTHORIZATION", true),
    ENABLE_OAUTH2_PROXY_AUTHORIZATION("ENABLE_OAUTH2_PROXY_AUTHORIZATION", true),
    USER_CONTEXT_SERVICE_URL("USER_CONTEXT_SERVICE_URL", false),
    DATABASE_TYPE("DATABASE_TYPE",false),
    CPR_SERVICE_URL("CPR_SERVICE_URL",true),
    BRUGS_SERVICE_URL("BRUGS_SERVICE_URL",true),
    BRUGSAFTALE_ENABLED("BRUGSAFTALE_ENABLED", true),
    ENABLE_CPR_SERVICE("ENABLE_CPR_SERVICE", true),
    BRUGSAFTALE_INTERNALIZED("BRUGSAFTALE_INTERNALIZED", true),
    OUTDATED_IF_OLDER_THAN_DAYS("OUTDATED_IF_OLDER_THAN_DAYS",true),
    OFFICIAL_BRUGSAFTALE_SYSTEM("OFFICIAL_BRUGSAFTALE_SYSTEM",true),
    OFFICIAL_THRESHOLDSET_IDENTIFIER_SYSTEM("OFFICIAL_THRESHOLDSET_IDENTIFIER_SYSTEM",true),
    OFFICIAL_THRESHOLDSET_APPLIESTO_URL("OFFICIAL_THRESHOLDSET_APPLIESTO_URL",true),
    OFFICIAL_CAREPLAN_IDENTIFIER_SYSTEM("OFFICIAL_CAREPLAN_IDENTIFIER_SYSTEM",true),
    OFFICIAL_PATIENT_IDENTIFIER_SYSTEM("OFFICIAL_PATIENT_IDENTIFIER_SYSTEM", true);

    private String key;
    private boolean required;

    ServiceVariables(String key, boolean required) {
        this.key = key;
        this.required = required;
    }

    public static void registerAndEnsurePresence() {
        List<String> requiredKeys = variablesWith(v -> v.required).stream().map(ServiceVariables::getKey).collect(Collectors.toList());
        Env.registerRequiredEnvVars(requiredKeys);
        Env.checkEnv();
    }

    private static List<ServiceVariables> variablesWith(Predicate<ServiceVariables> predicate) {
        return Arrays.stream(ServiceVariables.values()).filter(predicate).collect(Collectors.toList());
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        String value = System.getenv(getKey());
        if (value == null || value.isEmpty()) {
            if (required) {
                throw new IllegalArgumentException("Required key not present: " + getKey());
            } else {
                return null;
            }
        }
        return value;
    }

    public boolean isSetToTrue() {
        String value = getValue();
        if (value == null) {
            return false;
        }
        return value.equalsIgnoreCase("true");
    }
}
