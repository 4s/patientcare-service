package dk.s4.microservices.patientcareservice.servlet;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.DaoConfig;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.searchparam.registry.ISearchParamRegistry;
import ca.uhn.fhir.parser.StrictErrorHandler;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.interceptor.ResponseHighlighterInterceptor;
import dk.s4.microservices.genericresourceservice.servlet.JpaServerGenericServlet;
import dk.s4.microservices.messaging.EventProcessor;
import dk.s4.microservices.messaging.MessagingInitializationException;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.kafka.KafkaConsumeAndProcess;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.messaging.kafka.KafkaInitializationException;
import dk.s4.microservices.microservicecommon.Env;
import dk.s4.microservices.microservicecommon.FhirTopics;
import dk.s4.microservices.microservicecommon.MetricsEventConsumerInterceptorAdaptor;
import dk.s4.microservices.microservicecommon.fhir.SearchParameterFacade;
import dk.s4.microservices.microservicecommon.security.*;
import dk.s4.microservices.patientcareservice.CprServiceInterface;
import dk.s4.microservices.patientcareservice.DiasCprService;
import dk.s4.microservices.patientcareservice.ServiceVariables;
import dk.s4.microservices.patientcareservice.health.HealthEndpoint;
import dk.s4.microservices.patientcareservice.messaging.MyEventProcessor;
import dk.s4.microservices.patientcareservice.messaging.ResourceDeleter;
import dk.s4.microservices.patientcareservice.provider.FHIRCarePlanResourceProvider;
import dk.s4.microservices.patientcareservice.provider.FHIRCprPatientResourceProvider;
import dk.s4.microservices.patientcareservice.provider.FHIRPatientResourceProvider;
import org.hl7.fhir.r4.model.Basic;
import org.hl7.fhir.r4.model.CarePlan;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.SearchParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static dk.s4.microservices.microservicecommon.fhir.ResourceUtil.getResourceStream;


/**
 * PatientCare Service.
 * <p>
 * It exposes a RESTful interface for:
 * <p>
 * - accessing FHIR Patient resources and FHIR Careplan resources
 * - accessing Threshold resources (as FHIR Basic resource) for a given Careplan and observation/questionnaire resources
 * <p>
 * The POST/PUT operations of the REST interface cannot be used
 * directly. Resources are created an updated via Kafka messages.
 */
public class PatientCareService extends JpaServerGenericServlet {

    private static final long serialVersionUID = 1L;

    private final static Logger logger = LoggerFactory.getLogger(PatientCareService.class);

    private static FhirContext fhirContext;
    private static IFhirResourceDao<Basic> basicDaoR4;
    private static IFhirResourceDao<CarePlan> carePlanDaoR4;
    private static IFhirResourceDao<Patient> patientDaoR4;
    private static DaoConfig daoConfig;
    private static ResourceDeleter<Patient> patientDeleter;
    private static ResourceDeleter<CarePlan> carePlanDeleter;

    private KafkaConsumeAndProcess kafkaConsumeAndProcess;
    private Thread kafkaConsumeAndProcessThread;
    private UserContextResolverInterface userResolver;

    public PatientCareService() {
        super(getMyFhirContext(), logger);
    }

    /**
     * Singleton FhirContext
     *
     * @return the FhirContext
     */
    private static FhirContext getMyFhirContext() {
        if (fhirContext == null) {
            fhirContext = FhirContext.forR4();
            fhirContext.setParserErrorHandler(new StrictErrorHandler());
        }
        return fhirContext;
    }

    @Override
    public UserContextResolverInterface getUserResolver() {
        if(userResolver == null) {
            if (ServiceVariables.ENABLE_DIAS_AUTHENTICATION.isSetToTrue()) {
                userResolver = new DiasUserContextResolver(ServiceVariables.USER_CONTEXT_SERVICE_URL.getValue());
            } else if (ServiceVariables.ENABLE_KEYCLOAK_GATEKEEPER_AUTHORIZATION.isSetToTrue()) {
                userResolver = new KeycloakGatekeeperUserContextResolver();
            } else if (ServiceVariables.ENABLE_OAUTH2_PROXY_AUTHORIZATION.isSetToTrue()) {
                userResolver = new OAuth2ProxyUserContextResolver();
            } else {
                userResolver = new DefaultUserContextResolver();
            }
        }
        return userResolver;
    }

    @Override
    public void destroy() {
        System.out.printf("Shutting down %s \n", ServiceVariables.SERVICE_NAME.getValue());
        if (kafkaConsumeAndProcess != null) {
            kafkaConsumeAndProcess.stopThread();
            try {
                kafkaConsumeAndProcessThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void registerAndCheckEnvironmentVars() {
        ServiceVariables.registerAndEnsurePresence();
    }

    @Override
    protected void initialize() throws ServletException {
        super.initialize();
        registerAndCheckEnvironmentVars();

        logger.debug("initialize");

		// Get the spring context from the web container (it's declared in web.xml)
        WebApplicationContext myAppCtx = ContextLoaderListener.getCurrentWebApplicationContext();

        patientDaoR4 = myAppCtx.getBean("myPatientDaoR4", IFhirResourceDao.class);
        basicDaoR4 = myAppCtx.getBean("myBasicDaoR4", IFhirResourceDao.class);
        carePlanDaoR4 = myAppCtx.getBean("myCarePlanDaoR4", IFhirResourceDao.class);

        daoConfig = myAppCtx.getBean(DaoConfig.class);

        IFhirResourceDao<SearchParameter> searchParameterDaoR4 = myAppCtx.getBean("mySearchParameterDaoR4", IFhirResourceDao.class);

        //Initiate Custom SearchParameters
        SearchParameterFacade searchParameterFacade = new SearchParameterFacade(searchParameterDaoR4,getMyFhirContext());
        try {
            searchParameterFacade.installSearchParameter(getResourceStream("CarePlanAuthorIdentifierSearchParam.json"));
            searchParameterFacade.installSearchParameter(getResourceStream("CarePlanSubjectIdentifierSearchParam.json"));
            searchParameterFacade.installSearchParameter(getResourceStream("PatientIdentifierSearchParam.json"));

            myAppCtx.getBean(ISearchParamRegistry.class).forceRefresh();
        } catch (IOException e) {
            throw new InternalError("Something went wrong while reading the custom SearchParameters");
        }

        /*
         * Add resource providers
         */
        List<IResourceProvider> resourceProviders = new ArrayList<>();
        if (ServiceVariables.ENABLE_CPR_SERVICE.isSetToTrue()) {
            CprServiceInterface cprService = new DiasCprService(ServiceVariables.CPR_SERVICE_URL.getValue());
            resourceProviders.add(new FHIRCprPatientResourceProvider(patientDaoR4, getMyFhirContext(), carePlanDaoR4, cprService));
        }
        else {
            resourceProviders.add(new FHIRPatientResourceProvider(patientDaoR4, getMyFhirContext(), carePlanDaoR4));
        }
        resourceProviders.add(new FHIRCarePlanResourceProvider(carePlanDaoR4, getMyFhirContext()));
        setResourceProviders(resourceProviders);

        /*
         * This server interceptor causes the server to return nicely formatter
         * and coloured responses instead of plain JSON/XML if the request is
         * coming from a browser window. It is optional, but can be nice for
         * testing.
         */
        registerInterceptor(new ResponseHighlighterInterceptor());

		/*
		 * This server interceptor causes the server to return nicely formatter
		 * and coloured responses instead of plain JSON/XML if the request is
		 * coming from a browser window. It is optional, but can be nice for
		 * testing.
		 */
		registerInterceptor(new ResponseHighlighterInterceptor());

        /*
         * Tells the server to return pretty-printed responses by default
         */
        setDefaultPrettyPrint(true);

        if (kafkaEnabled()) {
            List<Topic> topics = Arrays.asList(
                    FhirTopics.create("Patient"),
                    FhirTopics.update("Patient"),

                    FhirTopics.create("CarePlan"),
                    FhirTopics.update("CarePlan"),

                    FhirTopics.create("Basic"),
                    FhirTopics.update("Basic"),

                    new Topic()
                            .setOperation(Topic.Operation.Delete)
                            .setDataCategory(Topic.Category.System)
                            .setDataType("ConsentDeleted")
            );

            try {
                KafkaEventProducer kafkaEventProducer = new KafkaEventProducer(ServiceVariables.SERVICE_NAME.getValue());
                patientDeleter = new ResourceDeleter<Patient>(patientDaoR4,daoConfig);
                carePlanDeleter = new ResourceDeleter<CarePlan>(carePlanDaoR4,daoConfig);
                EventProcessor eventProcessor = new MyEventProcessor(getMyFhirContext(), basicDaoR4, carePlanDaoR4, patientDaoR4, patientDeleter, carePlanDeleter);
                kafkaConsumeAndProcess = new KafkaConsumeAndProcess(topics, kafkaEventProducer, eventProcessor);
                kafkaConsumeAndProcess.registerInterceptor(new MetricsEventConsumerInterceptorAdaptor());
                if (Env.isSetToTrue("ENABLE_DIAS_AUTHENTICATION")) {
                    kafkaConsumeAndProcess.registerInterceptor(new DiasEventConsumerInterceptor());
                }
                kafkaConsumeAndProcessThread = new Thread(kafkaConsumeAndProcess);
                kafkaConsumeAndProcessThread.start();
                HealthEndpoint.registerKafkaConsumeAndProcess(kafkaConsumeAndProcess);

            } catch (KafkaInitializationException | MessagingInitializationException e) {
                logger.error("Error during Kafka initialization: " + e.getMessage(), e);
                System.exit(1);
            }
        }
        logger.info("{} initialized", ServiceVariables.SERVICE_NAME.getValue());
    }

    private boolean kafkaEnabled(){
        String enableKafkaString = ServiceVariables.ENABLE_KAFKA.getValue();
        logger.debug("Read ENABLE_KAFKA from environment: " + enableKafkaString);
        return (enableKafkaString != null && enableKafkaString.equals("true"));
    }

    /**
     * For test
     */
    static IFhirResourceDao<CarePlan> getCarePlanDao() {
        if (carePlanDaoR4 != null) {
            return carePlanDaoR4;
        }
        throw new RuntimeException("carePlanDaoR4 dao not initialized");
    }

    /**
     * For test
     */
    static IFhirResourceDao<Patient> getPatientDao() {
        if (patientDaoR4 != null) {
            return patientDaoR4;
        }
        throw new RuntimeException("patientDaoR4 dao not initialized");
    }

    /**
     * For testing
     */
    static DaoConfig getDaoConfig() {
        if (daoConfig != null) {
            return daoConfig ;
        }
        throw new RuntimeException("Dao config not initialized");
    }

    /**
     * For test
     */
    static IFhirResourceDao<Basic> getBasicDao() {
        if (basicDaoR4 != null) {
            return basicDaoR4;
        }
        throw new RuntimeException("basicDaoR4 dao not initialized");
    }

    /**
     * For testing
     */
    static FhirContext getServerFhirContext() {
        return getMyFhirContext();
    }
}
