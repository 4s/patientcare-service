package dk.s4.microservices.patientcareservice.health;

import dk.s4.microservices.messaging.kafka.KafkaConsumeAndProcess;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import dk.s4.microservices.patientcareservice.ServiceVariables;
import org.json.simple.JSONObject;

/**
 * Exposes web service endpoint for checking overall health of the service.
 */
@Path("/")
public class HealthEndpoint {

	static private KafkaConsumeAndProcess kafkaConsumeAndProcess;

	static public void registerKafkaConsumeAndProcess(KafkaConsumeAndProcess kcap) {
		kafkaConsumeAndProcess = kcap;
	}
	/**
	 * Web service endpoint for checking overall health of the service.
	 *
	 * @return response
	 */
	@GET
	@Path("/")
	public Response getMsg() {
		if (ServiceVariables.ENABLE_KAFKA.isSetToTrue()) {
			if (kafkaConsumeAndProcess == null || !kafkaConsumeAndProcess.isHealthy()) {
				ResponseBuilder builder = Response.serverError();
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("status", "Kafka consumer is not healthy");
				builder.entity(jsonObject.toJSONString());
				return builder.build();
			}
		}
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("status", "all good");
		jsonObject.put("version", "5.1.1");
		return Response.ok(jsonObject.toJSONString(), MediaType.APPLICATION_JSON).build();
	}
}