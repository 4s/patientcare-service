package dk.s4.microservices.patientcareservice;

import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.hl7.fhir.r4.model.Address;
import org.hl7.fhir.r4.model.Address.AddressUse;
import org.hl7.fhir.r4.model.BooleanType;
import org.hl7.fhir.r4.model.Enumerations;
import org.hl7.fhir.r4.model.HumanName;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.StringType;

/**
 * Contains functionality for retrieving patient JSON object from the RM DIAS CPR-service and converting it to a
 * FHIR Patient resource
 */
public class DiasCprService implements CprServiceInterface {

    private String serviceUrl;

    /**
     *
     * @param serviceUrl URL of CPR-service
     */
    public DiasCprService(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    /**
     * Retrieves a patient JSON object from the CPR-service and converts it to a FHIR Patient resource.
     *
     * @param identifier CPR of the patient
     * @return The retrieved patient, returned as a FHIR Patient resource.
     * @throws ParseException exception
     */
    public Patient getPatient(String identifier) throws ParseException {

        JsonObject patientJson = null;
        try {
            patientJson = getPatientJson(identifier);
        }
        catch (IOException e) {
            throw new ResourceNotFoundException("A resource with the given identifier was not found.");
        }
        Patient retPatient = new Patient();

        if(!patientJson.has("identifier")){
            throw new ResourceNotFoundException("A resource with the given identifier was not found.");
        }

        // Set the identifier of the patient according to the json.
        if (patientJson.has("identifier")) {
            retPatient.addIdentifier(new Identifier().setSystem(ServiceVariables.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.getValue()).setValue(patientJson.get("identifier").getAsJsonArray().get(0).getAsJsonObject().get("id").getAsString()));
        }
        // Set the active to true
        retPatient.setActive(true);

        // Set the name of the patient according to the json.
        if (patientJson.has("name")) {
            HumanName name = new HumanName();
            JsonObject nameObject = patientJson.get("name").getAsJsonArray().get(0).getAsJsonObject();
            if (nameObject.has("family")) {
                name.setFamily(nameObject.get("family").getAsString());
            }
            if (nameObject.has("given")) {
                JsonArray names = nameObject.get("given").getAsJsonArray();
                for (JsonElement givenNameElm: names)
                    name.addGiven(givenNameElm.getAsString());
            }
            retPatient.addName(name);
        }

        // Set the gender of the patient according to the json.
        if (patientJson.has("gender")) {
            if (patientJson.get("gender").getAsString().equals("female")) {
                retPatient.setGender(Enumerations.AdministrativeGender.FEMALE);
            } else if (patientJson.get("gender").getAsString().equals("male")) {
                retPatient.setGender(Enumerations.AdministrativeGender.MALE);
            } else {
                retPatient.setGender(Enumerations.AdministrativeGender.UNKNOWN);
            }
        } else {
            retPatient.setGender(Enumerations.AdministrativeGender.UNKNOWN);
        }

        // Set the birthDate of the patient according to the json.
        if (patientJson.has("birthDate")) {
            String dateString = patientJson.get("birthDate").getAsString();
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            Date patientDateOfBirth = format.parse(dateString);
            retPatient.setBirthDate(patientDateOfBirth);
        }

        // Set the deceasedBoolean of the patient according to the json.
        if (patientJson.has("deceasedBoolean")) {
            String deceasedString = patientJson.get("deceasedBoolean").getAsString();
            if (deceasedString.equals("false")) {
                retPatient.setDeceased(new BooleanType(false));
            } else {
                retPatient.setDeceased(new BooleanType(true));
            }
        }

        // Set the address of the patient according to the json.
        if (patientJson.has("address")) {
            JsonObject jsonAddress = patientJson.get("address").getAsJsonArray().get(0).getAsJsonObject();
            Address address = new Address();
            address.setUse(Address.AddressUse.HOME);
            List<StringType> line = new ArrayList<>();
            if (jsonAddress.has("line")) {
                JsonArray lines = jsonAddress.get("line").getAsJsonArray();
                for (JsonElement lineElm: lines)
                    address.addLine(lineElm.getAsString());
            }
            if (jsonAddress.has("city")) {
                address.setCity(jsonAddress.get("city").getAsString());
            }
            if (jsonAddress.has("postalCode")) {
                address.setPostalCode(jsonAddress.get("postalCode").getAsString());
            }
            if (jsonAddress.has("country")) {
                address.setCountry(jsonAddress.get("country").getAsString());
            }
            address.setUse(AddressUse.HOME);

            retPatient.addAddress(address);
        }
        return retPatient;
    }

    private JsonObject getPatientJson(String identifier) throws IOException {
        String searchUrl = serviceUrl + "/Patient/" + identifier;

        // Look up patient in CPR component service
        URL url = new URL(searchUrl);
        URLConnection request = url.openConnection();
        request.connect();

        // Convert to a JSON object
        JsonParser jp = new JsonParser(); //from gson
        JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent())); //Convert the input stream to a json element
        JsonObject rootObject = root.getAsJsonObject(); //This is always an object
        return (JsonObject) rootObject.get("patient");
    }

}
