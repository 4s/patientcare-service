package dk.s4.microservices.patientcareservice;

import java.io.IOException;
import java.text.ParseException;
import org.hl7.fhir.r4.model.Patient;

/**
 *  Interface for retrieving patient JSON object from the RM DIAS CPR-service and converting it to a
 *  FHIR Patient resource
 *
 */
public interface CprServiceInterface {
    /**
     * Retrieves a patient JSON object from the CPR-service and converts it to a FHIR Patient resource.
     *
     * @param identifier CPR of the patient
     * @return The retrieved patient, returned as a FHIR Patient resource.
     * @throws IOException exception
     * @throws ParseException exception
     */
    public Patient getPatient(String identifier) throws ParseException, IOException;

}
