package dk.s4.microservices.patientcareservice.messaging;

import ca.uhn.fhir.jpa.dao.DaoConfig;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import ca.uhn.fhir.jpa.util.ExpungeOptions;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.rest.param.TokenParam;
import org.apache.commons.lang3.NotImplementedException;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.Patient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;

import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Class used to delete patient for a given user in a new thread.
 */
public class ResourceDeleter<T extends IBaseResource> {

    private final static Logger logger = LoggerFactory.getLogger(ResourceDeleter.class);
    private final IFhirResourceDao<T> resourceDao;
    private final String searchParameterName;
    private final DaoConfig theDaoConfig;
    private final ExpungeOptions expungeOptions;
    private final String OFFICIAL_PATIENT_IDENTIFIER_SYSTEM;
    private final ExecutorService threadPoolExecutor;

    public ResourceDeleter(IFhirResourceDao<T> resourceDao, DaoConfig theDaoConfig) {
        this.resourceDao = resourceDao;
        this.theDaoConfig = theDaoConfig;

        this.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM = System.getenv("OFFICIAL_PATIENT_IDENTIFIER_SYSTEM");
        if (OFFICIAL_PATIENT_IDENTIFIER_SYSTEM == null || OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.isEmpty()) {
            throw new RuntimeException("OFFICIAL_PATIENT_IDENTIFIER_SYSTEM must be set.");
        }
        searchParameterName = getSearchParameterNameForFHIRResource(resourceDao.getResourceType().getSimpleName());

        expungeOptions = new ExpungeOptions()
                .setExpungeDeletedResources(true)
                .setExpungeOldVersions(true);
        threadPoolExecutor = Executors.newSingleThreadExecutor(new CustomizableThreadFactory("delete-patient-"));

    }

    /**
     * Deletes resources for a user given their identifier.
     * The deletion is done in a new thread consecutive calls to the method are queued.
     *
     * @param userId The identifier of the user whose resources are to be deleted.
     */
    public Future<?> deleteResourceByUserIdentifier(String userId) {

        return threadPoolExecutor.submit(() -> {
            String resourceType = resourceDao.getResourceType().getSimpleName();
            logger.debug("Deleting " + resourceType);
            logger.trace("Deleting " + resourceType + "for user " + userId);
            Set<Long> longs = resourceDao.searchForIds(new SearchParameterMap
                    (searchParameterName, new TokenParam(OFFICIAL_PATIENT_IDENTIFIER_SYSTEM, userId)));

            boolean expungeEnabled = theDaoConfig.isExpungeEnabled();
            theDaoConfig.setExpungeEnabled(true);

            for(Long l : longs){
                IIdType iidType = new IdDt(l);
                resourceDao.delete(iidType);
                resourceDao.expunge(iidType, expungeOptions);
            }

            logger.debug("Successfully deleted " + resourceType);
            logger.trace("Successfully deleted " + resourceType + "for user " + userId);

            theDaoConfig.setExpungeEnabled(expungeEnabled);
        });

    }

    private static String getSearchParameterNameForFHIRResource(String resourceType) {
        logger.debug("God: " + resourceType);
        switch (resourceType) {
            case "Patient": return "patientIdentifier";
            case "CarePlan": return "subjectIdentifier";
            default: throw new NotImplementedException("Resource deletion not implemented for " + resourceType);
        }
    }

}
