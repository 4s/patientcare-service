package dk.s4.microservices.patientcareservice.messaging;

import dk.s4.microservices.microservicecommon.fhir.ExceptionUtils;
import dk.s4.microservices.patientcareservice.ServiceVariables;
import org.hl7.fhir.r4.model.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ResourceValidator {

    private ResourceValidator() {

    }

    public static boolean validateContainedThresholdSets(DomainResource resource) {
        List<Extension> extensions = extractExtensionsFromBasic(resource.getContained(), ServiceVariables.OFFICIAL_THRESHOLDSET_APPLIESTO_URL.getValue());
        List<String> canonicals = extractCanonicals(extensions);
        return !thereAreCollisions(canonicals);
    }

    static List<String> extractCanonicals(List<Extension> extensions) {
        List<String> canonicals = new ArrayList<>();
        for (Extension extension : extensions) {
            CanonicalType canonicalType = (CanonicalType) extension.getValue();
            canonicals.add(excludeVersion(canonicalType.getValue()));
        }
        return canonicals;
    }

    static String excludeVersion(String string) {
        if (string.contains("|")){
            return string.substring(0, string.indexOf("|"));
        } else {
            return string;
        }
    }

    static List<Extension> extractExtensionsFromBasic(List<Resource> contained, String url) {
        List<Extension> extensions = new ArrayList<>();
        for (Resource resource : contained) {
            if (resource instanceof Basic) {
                Extension extension = ((Basic) resource).getExtensionByUrl(url);
                if (extension == null) {
                    throw ExceptionUtils.fatalFormatException("The ThresholdSet contained no appliesTo field");
                }
                extensions.add(extension);
            }
        }
        return extensions;
    }

    static boolean thereAreCollisions(List<String> canonicals) {
        Set<String> set = new HashSet<>(canonicals);
        return set.size() < canonicals.size();
    }
}
