package dk.s4.microservices.patientcareservice.messaging;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import ca.uhn.fhir.rest.param.TokenParam;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.microservicecommon.fhir.FhirCUDEventProcessor;
import dk.s4.microservices.patientcareservice.ServiceVariables;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.Duration;
import java.time.Instant;
import java.util.*;

public class MyEventProcessor extends FhirCUDEventProcessor {

    private final static Logger logger = LoggerFactory.getLogger(MyEventProcessor.class);
    private final FhirContext fhirContext;
    private final IFhirResourceDao<Basic> basicDaoR4;
    private final IFhirResourceDao<CarePlan> carePlanDaoR4;
    private final IFhirResourceDao<Patient> patientDaoR4;
    private ResourceDeleter patientDeleter;
    private ResourceDeleter carePlanDeleter;


    public MyEventProcessor(FhirContext fhirContext, IFhirResourceDao<Basic> basicDaoR4,
                            IFhirResourceDao<CarePlan> carePlanDaoR4, IFhirResourceDao<Patient> patientDaoR4, ResourceDeleter<Patient> patientDeleter, ResourceDeleter<CarePlan> carePlanDeleter) {
        super(fhirContext);
        this.fhirContext = fhirContext;
        this.basicDaoR4 = basicDaoR4;
        this.carePlanDaoR4 = carePlanDaoR4;
        this.patientDaoR4 = patientDaoR4;
        this.patientDeleter = patientDeleter;
        this.carePlanDeleter = carePlanDeleter;

    }

    @Override
    protected IFhirResourceDao daoForResourceType(String resourceType) {
        if (resourceType.equals("Basic"))
            return basicDaoR4;
        if (resourceType.equals("Patient"))
            return patientDaoR4;
        if (resourceType.equals("CarePlan"))
            return carePlanDaoR4;

        logger.error("No DAO for resource type - should not happen");
        return null;
    }

    @Override
    protected Identifier identifierForResource(IBaseResource resource) {
        Identifier identifier = null;
        if (resource instanceof Basic) {
            Basic basic = (Basic) resource;
            identifier = basic.getIdentifier().stream()
                    .filter(ident -> ident.getSystem().equals(ServiceVariables.OFFICIAL_THRESHOLDSET_IDENTIFIER_SYSTEM.getValue()))
                    .findAny()
                    .orElse(null);
        } else if (resource instanceof Patient) {
            Patient patient = (Patient) resource;
            identifier = patient.getIdentifier().stream()
                    .filter(ident -> ident.getSystem().equals(ServiceVariables.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.getValue()))
                    .findAny()
                    .orElse(null);
        } else if (resource instanceof CarePlan) {
            CarePlan carePlan = (CarePlan) resource;
            identifier = carePlan.getIdentifier().stream()
                    .filter(ident -> ident.getSystem().equals(ServiceVariables.OFFICIAL_CAREPLAN_IDENTIFIER_SYSTEM.getValue()))
                    .findAny()
                    .orElse(null);
        }
        return identifier;
    }

    /**
     * Process the incoming Create or Update message.
     */
    @Override
    protected boolean processCreateOrUpdate(Topic consumedTopic,
            Message receivedMessage,
            Topic messageProcessedTopic,
            Message outgoingMessage) {
        logger.debug("processCreateOrUpdate: " + consumedTopic);
        String bodyType = receivedMessage.getBodyType();
        if (bodyType.equals("CarePlan")) {
            CarePlan inputResource = (CarePlan) fhirContext.newJsonParser().parseResource(receivedMessage.getBody());
            if (consumedTopic.getOperation() == Topic.Operation.Create) {
                return processCreateMessage(consumedTopic, inputResource, receivedMessage, messageProcessedTopic, outgoingMessage);
            } else {
                return processUpdateMessage(consumedTopic, inputResource, receivedMessage, messageProcessedTopic, outgoingMessage);
            }
        } else {
            return super.processCreateOrUpdate(consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage);
        }
    }



    private boolean processUpdateMessage(Topic consumedTopic, CarePlan carePlan, Message receivedMessage, Topic messageProcessedTopic, Message outgoingMessage) {
        if (carePlan.getStatus().toCode().equals("completed")) {
            return super.processCreateOrUpdate(consumedTopic,
                    receivedMessage.setBody(fhirContext.newJsonParser().encodeResourceToString(addDateToCarePlan(carePlan))),
                    messageProcessedTopic, outgoingMessage);
        }

        if (!ResourceValidator.validateContainedThresholdSets(carePlan)) {
            logger.debug("The CarePlan contains multiple ThresholdSets that reference the same questionnaire");
            super.buildErrorMessage(receivedMessage, outgoingMessage,
                    "The CarePlan contains multiple ThresholdSets that reference the same questionnaire");
            return false;
        } else {
            return super.processCreateOrUpdate(consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage);
        }
    }

    public static CarePlan addDateToCarePlan(CarePlan carePlan) {
        Date date = new Date();
        return carePlan.setPeriod(carePlan.getPeriod().setEnd(date));
    }

    @Override
    public boolean processDelete(Topic consumedTopic, Message receivedMessage, Topic messageProcessedTopic, Message outgoingMessage) {

        // delete if consent has been withdrawn.
        if (consumedTopic.getDataType().equals("ConsentDeleted")){

            ObjectNode messageBody;

            try{
                messageBody = new ObjectMapper().readValue(receivedMessage.getBody(), ObjectNode.class);
            }
            catch (Exception e){
                return false;
            }

            String userId = messageBody.get("userId").toString().replace("\"", "");

            deletePatient(userId);
            deleteCarePlan(userId);

            messageProcessedTopic.setOperation(Topic.Operation.ProcessingOk);
            OperationOutcome operationOutcome = new OperationOutcome();
            operationOutcome.addIssue()
                    .setSeverity(OperationOutcome.IssueSeverity.INFORMATION)
                    .setDetails(new CodeableConcept().
                            setText("Successfully deleted observations for user: " + userId));

            outgoingMessage.setBodyType("OperationOutcome")
                    .setBodyCategory(Message.BodyCategory.System)
                    .setContentVersion(receivedMessage.getContentVersion())
                    .setSender(System.getenv("SERVICE_NAME"))
                    .setBody(fhirContext.newJsonParser()
                            .encodeResourceToString(operationOutcome));

            return true;

        }

        return false;
    }

    protected void deletePatient(String userId){ patientDeleter.deleteResourceByUserIdentifier(userId); }
    protected void deleteCarePlan(String userId){ carePlanDeleter.deleteResourceByUserIdentifier(userId); }

    /**
     * Process a create message, for a careplan.
     *
     * @param consumedTopic the consumed topic
     * @param carePlan        the careplan in the message
     * @param receivedMessage the received message
     * @param messageProcessedTopic messageProcessedTopic
     * @param outgoingMessage the outgoing message
     *
     */
    public boolean processCreateMessage(Topic consumedTopic, CarePlan carePlan, Message receivedMessage, Topic messageProcessedTopic, Message outgoingMessage) {
        if (carePlan.getStatus().equals(CarePlan.CarePlanStatus.ACTIVE)) {
            logger.debug("processCreateMessage: it's an active CarePlan");
            if (thereExistsOtherActiveCarePlans(carePlan)) {
                logger.error("processCreateMessage: The patient is already connected to an active CarePlan, for that organization");
                buildErrorMessage(receivedMessage, outgoingMessage, "The patient is already connected to an active CarePlan, for that organization");
                return false;
            }
            if (ServiceVariables.BRUGSAFTALE_ENABLED.isSetToTrue()) {
                try {
                    Instant start = Instant.now();
                    String brugsAftaleIdentifier = createBrugsForCarePlan(carePlan.getIdentifier().get(0).getValue());
                    carePlan.addIdentifier(new Identifier().setSystem(System.getenv("OFFICIAL_BRUGSAFTALE_SYSTEM"))
                            .setValue(brugsAftaleIdentifier));
                    logger.info("Brugsaftale creation took: " + Duration.between(start, Instant.now()).toMillis() + " milliseconds");
                } catch (IOException e) {
                    logger.error("processCreateMessage: IOException error " + e.getMessage(), e);
                    buildErrorMessage(receivedMessage, outgoingMessage, e.getMessage());
                    return false;
                }
            }
            receivedMessage.setBody(fhirContext.newJsonParser().encodeResourceToString(carePlan));
        }
        return super.processCreateOrUpdate(consumedTopic, receivedMessage, messageProcessedTopic, outgoingMessage);
    }


    /**
     * Checks in the dao if there exists other active careplans for the same subject, and author, as the given careplan.
     *
     * @param carePlan the CarePlan
     * @return boolean
     */
    private boolean thereExistsOtherActiveCarePlans(CarePlan carePlan) {
        SearchParameterMap paramMap = constructParamMapForCarePlan(carePlan);

        IBundleProvider carePlanBundleProvider = carePlanDaoR4.search(paramMap);

        //There exists other active careplans
        return carePlanBundleProvider.size() > 0;
    }

    /**
     * Constructs a param map for searching the dao for active careplans matching the given careplans subject, performer and status.
     *
     * @param carePlan the careplan
     * @return a param map
     */
    public static SearchParameterMap constructParamMapForCarePlan(CarePlan carePlan) {
        SearchParameterMap paramMap = new SearchParameterMap();
        TokenParam subjectIdentifier = new TokenParam(carePlan.getSubject().getIdentifier().getSystem(), carePlan.getSubject().getIdentifier().getValue());
        TokenParam authorIdentifier = new TokenParam(carePlan.getAuthor().getIdentifier().getSystem(), carePlan.getAuthor().getIdentifier().getValue());
        TokenParam activeStatus = new TokenParam().setValue("ACTIVE");

        // Search for CarePlans with same subject, author, and which is active.
        paramMap.add("subjectIdentifier", subjectIdentifier);
        paramMap.add("authorIdentifier", authorIdentifier);
        paramMap.add(CarePlan.SP_STATUS, activeStatus);

        return paramMap;
    }


    /**
     * Creates a Brugsaftale in the brugsaftale-service, by making a post-request to the service with the given identifier.
     * Returns the Brugsaftale-Identifier of the new brugsaftale as a String.
     *
     * @param identifier the identifier of the careplan
     * @return string containing the value of the brugsaftale-identifier
     */
    public static String createBrugsForCarePlan(String identifier) throws IOException {
        logger.trace("createBrugsForCarePlan: identifier = " + identifier);

        if (ServiceVariables.BRUGSAFTALE_INTERNALIZED.isSetToTrue()) {
            return UUID.randomUUID().toString();
        }
        else {
            String searchUrl = ServiceVariables.BRUGS_SERVICE_URL.getValue() + "api/" + identifier;
            logger.trace("createBrugsForCarePlan: Doing POST to url = " + searchUrl);

            try {
                URL url = new URL(searchUrl);
                HttpURLConnection request = (HttpURLConnection) url.openConnection();
                logger.trace(("1"));
                request.setRequestMethod("POST");
                logger.trace(("2"));
                request.setDoOutput(true);
                logger.trace(("3"));
                request.connect();

                logger.debug("createBrugsForCarePlan: request response code = " + request.getResponseCode());

                if (request.getResponseCode() != 201) {
                    throw new IOException("BrugsAftale creation failed: " + request.getResponseMessage());
                }

                try (BufferedReader in = new BufferedReader(new InputStreamReader(request.getInputStream()))) {
                    String inputLine;
                    StringBuilder response = new StringBuilder();

                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    JsonParser jp = new JsonParser();
                    JsonElement root = jp.parse(response.toString());
                    JsonObject rootObject = root.getAsJsonObject();

                    return rootObject.get("id").getAsString();
                }
            } catch (Exception e) {
                logger.error("Failed sending request to brugsaftale-service: " + e.getMessage(), e);
                throw e;
            }
        }
    }
}
