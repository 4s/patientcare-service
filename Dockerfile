FROM jetty:9-jre11
USER jetty:jetty
MAINTAINER michael.christensen@alexandra.dk

LABEL Description="patientcare-service" Vendor="Alexandra Institute A/S"

WORKDIR /opt/s4/patientcare-service
ADD https://bitbucket.org/4s/ms-scripts/raw/HEAD/wait-for-it.sh .
USER root
RUN ["chmod", "777", "wait-for-it.sh"]
USER jetty:jetty

ADD ./target/service.war /var/lib/jetty/webapps/root.war

WORKDIR /var/lib/jetty

RUN wget https://downloads.jboss.org/keycloak/3.1.0.Final/adapters/keycloak-oidc/keycloak-jetty94-adapter-dist-3.1.0.Final.tar.gz

RUN tar xvzf keycloak-jetty94-adapter-dist-3.1.0.Final.tar.gz
RUN rm keycloak-jetty94-adapter-dist-3.1.0.Final.tar.gz

EXPOSE 8080

#CMD [ "java", "-jar", "/usr/local/jetty/start.jar", "--create-startd", "--add-to-start=keycloak"]

RUN java -jar $JETTY_HOME/start.jar --create-startd --add-to-start=keycloak

ENV JAVA_OPTIONS ""
ENTRYPOINT ["/bin/bash", "-c", "java ${JAVA_OPTIONS} -XX:+PrintFlagsFinal -XX:+UseContainerSupport -jar /usr/local/jetty/start.jar"]